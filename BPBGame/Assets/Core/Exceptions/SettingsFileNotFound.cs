using System;
using UnityEngine;

namespace Core.Exceptions
{
    public class SettingsFileNotFound : Exception
    {
        public SettingsFileNotFound(string fileName) 
            : base(Application.persistentDataPath + "/"+ fileName + " not found")
        {
        }
    }
}