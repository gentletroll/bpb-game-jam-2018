using System;

namespace Core.Exceptions
{
    public class ResourcesFileNotFound : Exception
    {
        public ResourcesFileNotFound(string fileName) 
            : base(fileName + " not found")
        {
        }
    }
}