using System;
using UnityEngine;

namespace Core.Exceptions
{
    public class TranslationFileNotFound : Exception
    {
        public TranslationFileNotFound(string languageCode) 
            : base("Could not load TextAsset " + languageCode)
        {
        }
    }

    public class UICanvasPrefabNotFound : Exception
    {
        public UICanvasPrefabNotFound() 
            : base("Could not load UICanvas.prefab in any resources folder")
        {
        }
    }
}