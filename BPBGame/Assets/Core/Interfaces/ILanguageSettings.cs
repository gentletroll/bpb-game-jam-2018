using UniRx;

namespace Core.Interfaces
{
    public interface ILanguageSettings
    {
        ReactiveProperty<string> TextLanguage { get; set; }
    }
}