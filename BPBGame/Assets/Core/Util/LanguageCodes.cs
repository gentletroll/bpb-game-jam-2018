namespace Core.Util
{
    public static class LanguageCodes
    {
        public const string German = "de_DE";
        public const string English = "en_UK";
        public const string American = "en_US";
        public const string French = "fr_FR";
    }
}