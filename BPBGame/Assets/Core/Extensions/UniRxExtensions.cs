using System;
using UniRx;
using UnityEngine.UI;

namespace Core.Extensions
{
    public static class UniRxExtensions
    {
        private static readonly TimeSpan SingleClickThrottleTime = TimeSpan.FromSeconds(0.1d);

        public static UniRx.IObservable<Unit> OnSingleClickAsObservable(this Button button, TimeSpan? throttleTime = null)
        {
            return throttleTime.HasValue ?
                button.OnClickAsObservable().ThrottleFirst(throttleTime.Value) :
                button.OnClickAsObservable().ThrottleFirst(SingleClickThrottleTime);
        }
    }
}