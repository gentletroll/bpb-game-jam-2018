namespace Core.Extensions
{
    public static class StringExtensions
    {
        public static bool IsNullOrWhiteSpace(this string s)
        {
            if ((s == null) || (s == " "))
                return true;

            return false;
        }
    }
}