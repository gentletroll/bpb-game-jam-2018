using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Extensions
{
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Shuffles all entries in the sequence. The original sequence is not changed.
        /// </summary>
        /// <param name="list">The source sequence.</param>
        /// <returns>Returns a new array with the shuffled entries.</returns>
        public static T[] Randomize<T>(this IEnumerable<T> list)
        {
            return list.Randomize<T>((int)DateTimeOffset.Now.Ticks);
        }

        /// <summary>
        /// Shuffles all entries in the sequence. The original sequence is not changed.
        /// </summary>
        /// <param name="list">The source sequence.</param>
        /// <param name="seed">The seed value for the <see cref="T:System.Random" /> class.</param>
        /// <returns>Returns a new array with the shuffled entries.</returns>
        public static T[] Randomize<T>(this IEnumerable<T> list, int seed)
        {
            System.Random random = new System.Random(seed);
            T[] array = list.ToArray<T>();
            for (int index1 = array.Length - 1; index1 > 0; --index1)
            {
                int index2 = random.Next(index1 + 1);
                EnumerableExtensions.Swap<T>(ref array[index1], ref array[index2]);
            }
            return array;
        }

        public static IEnumerable<IEnumerable<T>> Batch<T>(this IEnumerable<T> source, int batchSize)
        {
            if (batchSize == 0)
            {
                yield return source;
            }
            else
            {
                List<T> batch = new List<T>(batchSize);
                foreach (T obj in source)
                {
                    batch.Add(obj);
                    if (batch.Count >= batchSize)
                    {
                        yield return (IEnumerable<T>)batch;
                        batch = new List<T>(batchSize);
                    }
                }
                if (batch.Count > 0)
                    yield return (IEnumerable<T>)batch;
            }
        }

        private static void Swap<T>(ref T element1, ref T element2)
        {
            T obj = element2;
            element2 = element1;
            element1 = obj;
        }
    }
}
