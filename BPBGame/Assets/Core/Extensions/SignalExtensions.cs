using IoCPlus;
using UniRx;

namespace Core.Extensions
{
    public static class SignalExtensions
    {
        public static IObservable<Unit> AsObservable(this Signal signal)
        {
            return Observable.FromEvent<System.Action>(h => h, signal.AddListener, signal.RemoveListener);
        }

        public static IObservable<T> AsObservable<T>(this Signal<T> signal)
        {
            return Observable.FromEvent<System.Action<T>, T>(h => h, signal.AddListener, signal.RemoveListener);
        }

        public static IObservable<Tuple<T0, T1>> AsObservable<T0, T1>(this Signal<T0, T1> signal)
        {
            return Observable.FromEvent<System.Action<T0, T1>, Tuple<T0, T1>>(h =>
            {
                return new System.Action<T0, T1>((t0, t1) =>
                {
                    h(Tuple.Create(t0, t1));
                });
            }, signal.AddListener, signal.RemoveListener);
        }
    }
}