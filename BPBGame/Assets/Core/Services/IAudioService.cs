using UnityEngine;

namespace Core.Services
{
    public interface IAudioService
    {
         AudioClip GetClip(string name);
    }
}