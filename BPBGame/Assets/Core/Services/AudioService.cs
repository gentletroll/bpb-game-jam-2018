using Core.Repositories;
using UnityEngine;

namespace Core.Services
{
    public class AudioService : IAudioService
    {
        private readonly IAudioClipRepository _audioClipRepository;

        public AudioService(IAudioClipRepository audioClipRepository)
        {
            _audioClipRepository = audioClipRepository;
        }

        public AudioClip GetClip(string name)
        {
            // NOTE: we can also cache the audio clips here. But first check performance.
            return _audioClipRepository.GetClip(name);
        }
    }
}