using System.IO;
using Core.Exceptions;
using Newtonsoft.Json;
using UnityEngine;

namespace Core.Services
{
    public abstract class SettingsService<T> : ISettingsService<T>
    {
        public abstract string SettingsFileName { get; }

        public T Load()
        {
            if (!File.Exists(Application.persistentDataPath + "/" + SettingsFileName))
                throw new SettingsFileNotFound(SettingsFileName);

            return JsonConvert.DeserializeObject<T>(GetJson(SettingsFileName));
        }

        public void Save(T settings)
        {
            var json = JsonConvert.SerializeObject(settings);
            
            var writer = new StreamWriter(Application.persistentDataPath  + "/" + SettingsFileName);

            writer.Write(json);
            writer.Close();             
         }

        private static string GetJson(string settingsFile)
        {
            using (var textReader = new StreamReader(Application.persistentDataPath + "/" + settingsFile))
            {
                return textReader.ReadToEnd();
            }           
        }
    }
}