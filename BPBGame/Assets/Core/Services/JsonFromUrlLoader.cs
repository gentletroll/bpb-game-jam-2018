using System;
using System.Collections;
using UniRx;

namespace Core.Services
{
    public class JsonFromUrlLoader : IJsonFromUrlLoader
    {
        public IObservable<string> LoadJson(string url)
        {
             return Observable
                .FromCoroutine<string>((observer, cancellationToken) => GetJsonByUrl(url, observer, cancellationToken));            
        }

        private static IEnumerator GetJsonByUrl(string url, IObserver<string> observer, CancellationToken cancellationToken)
        {
            var www = new UnityEngine.WWW(url);
            while (!www.isDone && !cancellationToken.IsCancellationRequested)
            {
                yield return null;
            }

            if (cancellationToken.IsCancellationRequested) yield break;

            if (www.error != null)
            {
                observer.OnError(new System.Exception(www.error + " " + www.text));
            }
            else
            {
                observer.OnNext(www.text);
                observer.OnCompleted();
            }
        }
    }
}