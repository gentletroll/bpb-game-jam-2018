using Core.Interfaces;
using Core.Repositories;

namespace Core.Services
{
    public class TextService : ITextService
    {
        private readonly ITextRepository _textRepository;
        private readonly ILanguageSettings _languageSettings;

        public TextService(ITextRepository textRepository, ILanguageSettings languageSettings)
        {
            _languageSettings = languageSettings;
            _textRepository = textRepository;
        }

        public string GetById(string textId)
        {
            var text = _textRepository.GetText(textId, _languageSettings.TextLanguage.Value);

            if (text != string.Empty)
                return text;

            return "Unknwon text id";
        }
    }
}