using UniRx;

namespace Core.Services
{
    public interface IJsonFromUrlLoader
    {
        IObservable<string> LoadJson(string url);
    }
}