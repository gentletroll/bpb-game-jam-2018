namespace Core.Services
{
    public interface ITextService
    {
         string GetById(string textId);
    }
}