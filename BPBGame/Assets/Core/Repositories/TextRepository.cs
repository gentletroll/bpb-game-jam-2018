using System;
using System.Collections.Generic;
using Core.Exceptions;
using Newtonsoft.Json;
using UnityEngine;

namespace Core.Repositories
{
    public class TextRepository : ITextRepository
    {
        private string _currentLanguage;

        private Dictionary<string, string> _langaugesTexts;

        public TextRepository()
        {
            _currentLanguage = string.Empty;            
        }

        public string GetText(string textId, string languageCode)
        {
            if (_currentLanguage != languageCode)                
            {
                var textAsset = Resources.Load<TextAsset>("Translations/" + languageCode);

                if (textAsset == null)
                    throw new TranslationFileNotFound(languageCode);
             
                _langaugesTexts = JsonConvert.DeserializeObject<Dictionary<string, string>>(textAsset.text);
                _currentLanguage = languageCode;
            }

            string text;
            if (!_langaugesTexts.TryGetValue(textId, out text))
                return textId + "* " + languageCode;

            return text;  
        }
    }
}