using UnityEngine;

namespace Core.Repositories
{
    public interface IAudioClipRepository
    {
        AudioClip GetClip(string name);
    }
}