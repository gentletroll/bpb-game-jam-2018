namespace Core.Repositories
{
    public interface ITextRepository
    {
        string GetText(string textId, string languageCode);
    }
}