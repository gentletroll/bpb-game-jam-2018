using System;
using UnityEngine;

namespace Core.Repositories
{
    public class ResourcesAudioClipRepository : IAudioClipRepository
    {
        private string _audioFolder;

        public ResourcesAudioClipRepository(string folder = "Sounds/")
        {
            _audioFolder = folder;                
        }

        public AudioClip GetClip(string name)
        {
            var audioClip = Resources.Load<AudioClip>(_audioFolder + name);

            if (audioClip == null)
                throw new Exception("Could not load audio clip");

            return audioClip;                        
        }
    }
}