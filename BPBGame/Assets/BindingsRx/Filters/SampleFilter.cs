﻿using System;
using UniRx;

namespace BindingsRx.Filters
{
    public class SampleFilter<T> : IFilter<T>
    {
        public TimeSpan SampleRate { get; set; }

        public SampleFilter(TimeSpan sampleRate)
        { SampleRate = sampleRate; }

        public UniRx.IObservable<T> InputFilter(UniRx.IObservable<T> inputStream)
        { return inputStream.Sample(SampleRate); }

        public UniRx.IObservable<T> OutputFilter(UniRx.IObservable<T> outputStream)
        { return outputStream.Sample(SampleRate); }
    }
}