/*
 * Copyright 2014, Gregg Tavares.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Gregg Tavares. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
"use strict";

// Start the main app logic.
var commonUI = sampleUI.commonUI;
var input = sampleUI.input;
var misc = sampleUI.misc;
var mobileHacks = sampleUI.mobileHacks;
var strings = sampleUI.strings;
var touch = sampleUI.touch;

var globals = {
  debug: false,
  //orientation: "landscape-primary",
};
misc.applyUrlSettings(globals);
mobileHacks.fixHeightHack();
mobileHacks.disableContextMenu();

var score = 0;
var choice = 0;
var statusElem = document.getElementById("gamestatus");
var inputElem = document.getElementById("inputarea");
var colorElem = document.getElementById("display");

var availableTokensElem = document.getElementById("availableTokens");
var numCoinsElem = document.getElementById("numCoins");

var headlineAgenda1Elem = document.getElementById("HeadlineAgenda1");
var headlineAgenda2Elem = document.getElementById("HeadlineAgenda2");

var textAgenda1Elem = document.getElementById("TextAgenda1");
var textAgenda2Elem = document.getElementById("TextAgenda2");

var overlayElem = document.getElementById("waitForPlayersOverlay");
var waitForVotingElem = document.getElementById("waitForVoting");

var agenda1IconElem = document.getElementById("agenda1Icon");
var agenda2IconElem = document.getElementById("agenda2Icon");
var partyMainIconElem = document.getElementById("partyMainIcon");

var client = new hft.GameClient();

commonUI.setupStandardControllerUI(client, globals);
commonUI.askForNameOnce();
commonUI.showMenu(true);

var randInt = function(range) {
  return Math.floor(Math.random() * range);
};

function to255(v) {
  return v * 255 | 0;
}

client.addEventListener('color', function(cmd) {
  // Pick a random color
  var c = cmd.color;
  var color =  'rgb(' + to255(c.r) + "," + to255(c.g) + "," + to255(c.b) + ")";
  colorElem.style.backgroundColor = color;
});

function increase() {

  choice--;
    
  if (Math.abs(choice) > score) 
  {
    choice = -score;
  }
  
  numCoinsElem.innerHTML = choice;
}

function decrease() 
{
  choice++;
  if (choice > score) 
  {
    choice = score;
  }
  
  numCoinsElem.innerHTML = choice;
}

function vote() 
{ 
  availableTokensElem.innerHTML = score - choice;

  waitForVotingElem.style.display = "block";

  client.sendCmd('vote', { tokens: choice });
}

client.addEventListener('onBegin', function(cmd) {

  partyMainIconElem.src = "images/partyIcons/" + cmd.partyid + ".png";

  waitForVotingElem.style.display = "none";
  overlayElem.style.display = "none";

  choice = 0;
  numCoinsElem.innerHTML = choice;
});


client.addEventListener('onEnd', function(cmd) {
  //availableTokensElem.innerHTML = 78237489234;
});


client.addEventListener('setTokens', function(cmd) {
  score = cmd.tokens;
  availableTokensElem.innerHTML = cmd.tokens;
});

client.addEventListener('setAgenda1', function(cmd) {
  // todo set icon
  var agenda1 = cmd.agenda;
  agenda1IconElem.src = "images/politicsIcons/" + agenda1 +"_full.png";
  headlineAgenda1Elem.innerHTML = cmd.headline;
  textAgenda1Elem.innerHTML = cmd.text;  
});

client.addEventListener('setAgenda2', function(cmd) {
  // todo set icon
  var agenda2 = cmd.agenda;
  agenda2IconElem.src = "images/politicsIcons/" + agenda2 +"_full.png";
  headlineAgenda2Elem.innerHTML = cmd.headline;
  textAgenda2Elem.innerHTML = cmd.text;  
});

var $ = document.getElementById.bind(document);
$("choice0").addEventListener('click', function() { increase(); });
$("choice1").addEventListener('click', function() { decrease(); });
$("btnVote").addEventListener('click', function() { vote(); });

//document.getElementById("imageid").src="../template/save.png";
