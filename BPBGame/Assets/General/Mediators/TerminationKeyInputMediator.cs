using System;
using Core.Extensions;
using General.Models;
using General.Signals;
using General.Views;
using IoCPlus;
using UniRx;
using UnityEngine;

namespace General.Mediators
{
    public class TerminationKeyInputMediator : Mediator<ITerminationKeyInputView>
    {
        [Inject]
        TerminationKeyPressedSignal _terminationKeyPressedSignal;

        [Inject]
        IGameBaseSettings  _gameBaseSettings;

        private IDisposable _observable;

        public override void Initialize()
        {
            _observable = Observable.EveryUpdate()
             .Where(_ => Input.GetKeyDown(_gameBaseSettings.TerminationKey))
             .Throttle(TimeSpan.FromMilliseconds(10))
             .Subscribe(xs => _terminationKeyPressedSignal.Dispatch());
        }

        public override void Dispose()
        {
            _observable.Dispose();
        }
    }
}