﻿using Core.Interfaces;
using General.Views;
using IoCPlus;
using General.Signals;
using Core.Services;
using UniRx;

namespace General.Mediators
{
    public class ButtonMediator : Mediator<IButtonView>
    {
        [Inject] 
        private ITextService _textService;
        
        [Inject]
        private ILanguageSettings _settings;
        
        [Inject] 
        private ButtonClickedSignal _buttonClickedSignal;
        
        public override void Initialize()
        {
            view.ButtonText = _textService.GetById(view.ButtonId);
            view.ButtonViewClickedSignal.AddListener(OnButtonClicked);
            
            _settings.TextLanguage.Do(x =>
                {
                    // update button text when text language was changed
                    view.ButtonText = _textService.GetById(view.ButtonId);
                })
                .Subscribe();
        }

        public override void Dispose()
        {
            view.ButtonViewClickedSignal.RemoveListener(OnButtonClicked);
        }

        private void OnButtonClicked()
        {
            _buttonClickedSignal.Dispatch(view);
        }
    }
}