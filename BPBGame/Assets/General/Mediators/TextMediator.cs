using Core.Interfaces;
using Core.Services;
using General.Views;
using IoCPlus;
using UniRx;

namespace General.Mediators
{
    public class TextMediator : Mediator<ITextView>
    {
        [Inject]
        private ILanguageSettings _languageSettings;

        [Inject]
        private ITextService _textService;

        public override void Initialize()
        {
            _languageSettings.TextLanguage
                .Subscribe(x => view.Text = _textService.GetById(view.TextId));
        }

        public override void Dispose()
        {
        }
    }
}