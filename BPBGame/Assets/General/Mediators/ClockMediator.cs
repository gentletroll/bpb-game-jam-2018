using System;
using General.Views;
using IoCPlus;
using UniRx;

namespace General.Mediators
{
    public class ClockMediator : Mediator<IClockView>
    {
        private IDisposable _timeObservable;

        public override void Initialize()
        {
            _timeObservable = Observable
                .EveryFixedUpdate()
                .Select(x => DateTime.Now.ToShortTimeString())
                .Subscribe(x => view.TimeText.text = x);
        }

        public override void Dispose()
        {
            _timeObservable.Dispose();
        }
    }
}