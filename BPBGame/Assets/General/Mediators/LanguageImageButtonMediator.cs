using Core.Interfaces;
using General.Views;
using IoCPlus;
using Core.Services;
using General.Signals;
using UniRx;
using UnityEngine;

namespace General.Mediators
{
    public class LanguageImageButtonMediator : Mediator<ILanguageImageButtonView>
    {
        [Inject]
        private ILanguageSettings _settings;
        
        [Inject]     
        private LanguageButtonClickedSignal _languageButtonClickedSignal;

        private Color _originalTextColor;

        public override void Initialize()
        {
            view.ButtonViewClickedSignal.AddListener(OnButtonClicked);

            _settings.TextLanguage.Do(language =>
            {
                view.Highlight(view.LanguageCode == language);
            }).Subscribe();
        }

        public override void Dispose()
        {
            view.ButtonViewClickedSignal.RemoveListener(OnButtonClicked);
        }

        private void OnButtonClicked()
        {
            _languageButtonClickedSignal.Dispatch(view.LanguageCode);
        }
    }
}