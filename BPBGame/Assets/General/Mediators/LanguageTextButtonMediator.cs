﻿using Core.Interfaces;
using General.Views;
using IoCPlus;
using Core.Services;
using General.Signals;
using UniRx;
using UnityEngine;

namespace General.Mediators
{
    public class LanguageTextButtonMediator : Mediator<ILanguageTextButtonView>
    {
        [Inject] 
        private ITextService _textService;
        
        [Inject]
        private ILanguageSettings _settings;
        
        [Inject]     
        private LanguageButtonClickedSignal _languageButtonClickedSignal;

        private Color _originalTextColor;

        public override void Initialize()
        {
            _originalTextColor = view.Text.color;            

            view.ButtonViewClickedSignal.AddListener(OnButtonClicked);

            _settings.TextLanguage.Do(language =>
            {
                view.Text.text = _textService.GetById(view.ButtonId);
                view.Text.color = _originalTextColor;

                if (view.LanguageCode == language)                
                    view.Text.color = new UnityEngine.Color(1,0,0);
                
            }).Subscribe();
        }

        public override void Dispose()
        {
            view.ButtonViewClickedSignal.RemoveListener(OnButtonClicked);
        }

        private void OnButtonClicked()
        {
            _languageButtonClickedSignal.Dispatch(view.LanguageCode);
        }
    }
}