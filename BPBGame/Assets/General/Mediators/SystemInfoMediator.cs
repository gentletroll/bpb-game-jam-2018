using System;
using System.Collections.Generic;
using System.Linq;
using Core.Humanizer.Bytes;
using Core.Interfaces;
using General.Views;
using IoCPlus;
using UniRx;
using UnityEngine;
using UnityEngine.Profiling;

namespace General.Mediators
{
    public class SystemInfoMediator : Mediator<ISystemInfoView>
    {
        [Inject]
        private ILanguageSettings _settings;
 
        private IDisposable _fpsObservable;
        private IDisposable _memoryObservable;

        public override void Initialize()
        {
            _fpsObservable = Observable                        
                .EveryUpdate()                
                .Select(x => (int)(1.0f / Time.deltaTime))
                 .Scan(new List<float>(), (buffer, value) => 
                { 
                    buffer.Add(value);
                    if(buffer.Count > 50)
                        buffer.RemoveAt(0);
                    return buffer;
                 })
                .Select(buffer => buffer.Average())
                .Select(x => string.Format("FPS: {0:N1}", x))                  
                .Sample(TimeSpan.FromMilliseconds(100))
                .SubscribeToText(view.Fps);

            _memoryObservable = Observable
                .Timer(TimeSpan.Zero, TimeSpan.FromMilliseconds(500))
                .Select(x => 
                    string.Format("Total: {0} Unity: {1} Mono: {2}",
                        (Profiler.usedHeapSizeLong + Profiler.GetMonoUsedSizeLong()).Bytes().ToString("MB"), 
                        Profiler.usedHeapSizeLong.Bytes().ToString("MB"), 
                        Profiler.GetMonoUsedSizeLong().Bytes().ToString("MB"))
                        )
                .SubscribeToText(view.Memory);        

            _settings.TextLanguage.Do(x=> UnityEngine.Debug.Log("Changed language")).SubscribeToText(view.Language);                        
        }

        public override void Dispose()
        {
            _fpsObservable.Dispose();
            _memoryObservable.Dispose();
        }
    }
}