using UnityEngine;

namespace General.Models
{
    public class GameBaseSettings : IGameBaseSettings
    {
        public KeyCode TerminationKey { get; set; }
    }
}