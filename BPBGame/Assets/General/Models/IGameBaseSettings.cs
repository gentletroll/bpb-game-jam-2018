using UnityEngine;

namespace General.Models
{
    public interface IGameBaseSettings
    {
        KeyCode TerminationKey { get; set; }
    }
}