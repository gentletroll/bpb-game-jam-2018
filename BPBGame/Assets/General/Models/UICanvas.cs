using UnityEngine;

namespace General.Models 
{ 
    [RequireComponent(typeof(Canvas))]
    public class UICanvas : MonoBehaviour, IUICanvas 
    {
        public void AddChild(GameObject gameObject) 
        {
            //var alala = gameObject.transform.GetChild(1).gameObject;
            // alala.transform.SetParent(transform, false);
            
            gameObject.transform.SetParent(transform.GetChild(1).transform, false);

            //gameObject.transform.SetParent(transform, false);
        }

        public void DestroyChild(GameObject gameObject)
        {
            gameObject.transform.SetParent(null);
            GameObject.Destroy(gameObject);
        }
    }
}