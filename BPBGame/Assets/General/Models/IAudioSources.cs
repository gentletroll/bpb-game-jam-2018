using General.Views;

namespace General.Models
{
    public interface IAudioSources
    {
        void AddSource(IAudioSourceView soundSource);
        IAudioSourceView GetByCategory(string category);
    }
}