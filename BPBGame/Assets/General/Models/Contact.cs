namespace General.Models
{
    public class Contact
    {
        public string Forename { get; set; }
        public string Surname { get; set; }
    }
}