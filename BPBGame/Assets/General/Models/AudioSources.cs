using System;
using System.Collections.Generic;
using System.Linq;
using General.Views;

namespace General.Models
{
    public class AudioSources : IAudioSources
    {
        private List<IAudioSourceView> _audioSources;

        public AudioSources()
        {
            _audioSources = new List<IAudioSourceView>();
        }

        public void AddSource(IAudioSourceView soundSource)
        {
            _audioSources.Add(soundSource);
        }

        public IAudioSourceView GetByCategory(string category)
        {
            var soundSource = _audioSources.SingleOrDefault(x => x.AudioCategory == category);

            if (soundSource == null)
                throw new Exception("Sound source for category " + category + " not found.");

            return soundSource;
        }
    }
}