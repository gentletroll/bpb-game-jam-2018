using UnityEngine;

namespace General.Models 
{
    public interface IUICanvas 
    {
        void AddChild(GameObject gameObject);
        void DestroyChild(GameObject gameObject);
    }
}