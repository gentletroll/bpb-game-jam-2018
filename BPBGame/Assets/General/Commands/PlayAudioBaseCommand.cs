using System;
using Core.Services;
using General.Models;
using IoCPlus;

namespace General.Commands
{
    public abstract class PlayAudioBaseCommand : Command
    {
        [Inject]
        IAudioSources _soundController;

        [Inject]
        IAudioService _audioService;

        public abstract string ClipName { get; }
        public abstract string Category { get; }

        protected override void Execute()
        {
            try
            {
                var clip = _audioService.GetClip(ClipName);

                var soundSource = _soundController.GetByCategory(Category);
                soundSource.Play(clip);
            }
            catch (Exception exception)
            {
                UnityEngine.Debug.LogException(exception);
            }
        }
    }
}


