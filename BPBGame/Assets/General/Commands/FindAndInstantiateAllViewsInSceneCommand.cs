using System.Linq;
using General.Components;
using IoCPlus;
using UnityEngine;
using UnityEngine.UI;

namespace General.Commands
{
    public class FindAndInstantiateAllViewsInSceneCommand : Command
    {
        [Inject] IContext context;
    
        protected override void Execute()
        {
            var containerBoundedViews = Object
                .FindObjectsOfType<View>()                
                .Where(x => x.GetComponent<ContainerBinding>() != null)
                .ToList();

            var childViews = containerBoundedViews
                .Select(x => x.gameObject)
                .SelectMany(x => x.GetComponentsInChildren<View>())
                .ToList();
 
            containerBoundedViews.AddRange(childViews);
 
            containerBoundedViews.ForEach(context.AddView);
        }
    }
}
