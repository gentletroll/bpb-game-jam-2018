using System;
using IoCPlus;
using UnityEngine;

namespace General.Commands.Development
{
    public class DumpTimestampCommand : Command
    {
        protected override void Execute()
        {
            Debug.Log(DateTime.Now.TimeOfDay.ToString());
        }              
    }
}