using IoCPlus;
using UnityEngine;

namespace General.Commands.Development
{
    public class HideDesignCanvasCommand : Command
    {
        protected override void Execute()
        {
            var designCanvas = GameObject.Find("Canvas");
            
            if (designCanvas == null)
                return;
            
            designCanvas.gameObject.SetActive(false);
        }              
    }
}