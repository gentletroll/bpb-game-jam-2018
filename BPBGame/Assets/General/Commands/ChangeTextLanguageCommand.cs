using Core.Interfaces;
using IoCPlus;

namespace General.Commands
{
    public class ChangeTextLanguageCommand : Command
    {
        [Inject] 
        private ILanguageSettings _languageSettings;
        
        [InjectParameter]
        private string _languageCode;
        
        protected override void Execute()
        {
            _languageSettings.TextLanguage.Value = _languageCode;
        }
    }
}