using General.Models;
using IoCPlus;
using UnityEngine;

namespace General.Commands
{
    public class InstantiateViewCommand : Command<string>
    {
        [Inject] IContext context;
        [Inject] UICanvases uiCanvases;
    
        protected override void Execute(string prefabName)
        {
            var prefab = Resources.Load<View>(prefabName);
            var mainMenuPanel = context.InstantiateView(prefab);

            uiCanvases.GUICanvas.AddChild(mainMenuPanel.GetGameObject());
        }
    }
}
