using IoCPlus;
using UnityEngine.SceneManagement;

namespace General.Commands
{
    public class LoadSceneCommand : Command<string>
    {
        protected override void Execute(string sceneName)
        {
            SceneManager.LoadScene(sceneName);
        }
    }
}
