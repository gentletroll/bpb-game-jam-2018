﻿using Core.Exceptions;
using General.Models;
using IoCPlus;
using UnityEngine;

namespace General.Commands
{
    public class InstantiateUICanvasCommand : Command
    {
        [Inject] 
        private UICanvases _canvases;
    
        protected override void Execute()
        {
            var prefab = Resources.Load<UICanvas>("UI Canvas 2");

            if (prefab == null)
                throw new UICanvasPrefabNotFound();

            _canvases.GUICanvas = Object.Instantiate(prefab);
        }
    }
}
