using Core.Exceptions;
using General.Models;
using General.Views;
using IoCPlus;
using UnityEngine;

namespace General.Commands
{
    public abstract class AddAudioSourceCommand<T> : Command 
        where T : AudioSourceView
    {
        [Inject]
        IContext _context;

        [Inject]
        IAudioSources _soundController;

        public abstract string AudioSourcePrefab { get; }
 
        protected override void Execute()
        {
            var prefab = Resources.Load<T>(AudioSourcePrefab);

             if (prefab == null)
                throw new ResourcesFileNotFound(AudioSourcePrefab);

            var soundSource = _context.InstantiateView(prefab);                            
            _soundController.AddSource(soundSource);
        }        
    }
}
