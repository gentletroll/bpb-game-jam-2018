using System;
using Core.Services;
using General.Models;
using IoCPlus;

namespace General.Commands
{
    public class PlayOneShotAudioCommand : Command<string, string>             
    {
        [Inject]
        IAudioSources _soundController;
        
        [Inject]
        IAudioService _audioService;

        protected override void Execute(string clipName, string category)
        {
            try 
            {
                var clip = _audioService.GetClip(clipName);

                var soundSource = _soundController.GetByCategory(category);                
                soundSource.PlayOneShot(clip);
            }
            catch (Exception exception)
            {
                UnityEngine.Debug.LogException(exception);
            }
        }        
    }
}
