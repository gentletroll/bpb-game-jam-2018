using General.Models;
using UniRx;

namespace General.Services
{
    public interface IRandomContactsService
    {         
        IObservable<Contact> GetRandomContacts(int count);
    }
}