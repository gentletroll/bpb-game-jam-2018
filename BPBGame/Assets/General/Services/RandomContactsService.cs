using System.Collections.Generic;
using System.Linq;
using Core.Services;
using General.Models;
using Newtonsoft.Json;
using UniRx;

namespace General.Services
{
    public class RandomContactsService : IRandomContactsService
    {
        private readonly IJsonFromUrlLoader _jsonFromUrlLoader;

        public RandomContactsService(IJsonFromUrlLoader urlLoader)
        {
            _jsonFromUrlLoader = urlLoader;
        }

        public IObservable<Contact> GetRandomContacts(int amount)
        {
            return Observable.Create<Contact>((x) =>
                                {
                                    _jsonFromUrlLoader.LoadJson("https://uinames.com/api/?amount=" + amount)
                                        .SelectMany(json =>
                                        {
                                            return JsonConvert.DeserializeObject<List<JsonContacts>>(json)
                                                .Select(info =>
                                                    new Contact
                                                    {
                                                        Forename = info.name,
                                                        Surname = info.surname
                                                    });
                                        })
                                        .Subscribe(contact => x.OnNext(contact), e => UnityEngine.Debug.LogError("RandomContactsService.GetRandomContacts("+ amount +") -> " + e.Message), () => x.OnCompleted());

                                    // we can use an empty disposable we have nothing which must be disposed after the use
                                    return Disposable.Create(() => UnityEngine.Debug.Log("Observer has unsubscribed"));
                                });
        }

        internal class JsonContacts
        {
            public string name;
            public string surname;
        }
    }
}