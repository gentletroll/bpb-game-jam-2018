﻿using General.Mediators;
using General.Models    ;
using General.Signals;
using General.Views;
using IoCPlus;
using UnityEngine;

namespace General.GameContext
{
    public abstract class GameContextBase : Context
    {
        protected override void SetBindings()
        {
            base.SetBindings();
            
            // models
            Bind<UICanvases>();
            Bind<IAudioSources>(new AudioSources());

            var gameBaseSettings = new GameBaseSettings
            {
                TerminationKey = KeyCode.Escape
            };

            Bind<IGameBaseSettings>(gameBaseSettings);

            // signals
            Bind<ButtonClickedSignal>();
            
            // views
            BindMediator<ButtonMediator, ButtonView>();
            BindMediator<TextMediator, TextView>();
            BindMediator<TerminationKeyInputMediator, TerminationKeyInputView>();
         }
    }
}