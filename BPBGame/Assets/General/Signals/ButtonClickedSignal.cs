using General.Views;
using IoCPlus;

namespace General.Signals
{
    public class ButtonClickedSignal : Signal<IButtonView>
    {                   
    }
}