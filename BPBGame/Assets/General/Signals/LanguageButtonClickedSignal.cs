using IoCPlus;

namespace General.Signals
{
    public class LanguageButtonClickedSignal : Signal<string>
    {                   
    }
}