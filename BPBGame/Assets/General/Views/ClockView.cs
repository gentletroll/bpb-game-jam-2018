using IoCPlus;
using UnityEngine;
using UnityEngine.UI;

namespace General.Views
{
    public class ClockView : View, IClockView
    {
        [SerializeField]
        Text _timeText;

        public Text TimeText { get { return _timeText; }}
    }
}