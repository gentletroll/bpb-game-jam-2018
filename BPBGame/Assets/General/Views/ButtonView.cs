using Core.Extensions;
using IoCPlus;
using UniRx;
using UnityEngine.UI;

namespace General.Views
{
    public class ButtonView : View, IButtonView
    {
        private Text _text;
        private Button _button;
        
        private readonly Signal _clickedSignal = new Signal();

        public Signal ButtonViewClickedSignal
        {
            get { return _clickedSignal; }
        }

        public string ButtonText
        {
            get { return _text.text; }
            set { _text.text = value; }
        }

        public string ButtonId
        {
            get { return _button.name; }
        }

        private void Awake()
        {
            _button = GetComponent<Button>();
            _text = GetComponentInChildren<Text>();
            
            _button.OnSingleClickAsObservable()
                .Subscribe(_ => _clickedSignal.Dispatch());
        }
    }
}