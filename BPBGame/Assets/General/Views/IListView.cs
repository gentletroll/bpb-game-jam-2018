using EnhancedUI.EnhancedScroller;
using IoCPlus;

namespace General.Views
{
    public interface IListView<T> : IListView
    {
        void Add(T data);
    }

    public interface IListView : IView, IEnhancedScrollerDelegate
    {
        void Reload();
    }
}