using Core.Extensions;
using IoCPlus;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace General.Views
{
    public class LanguageImageButtonView : View, ILanguageImageButtonView
    {
        [SerializeField]
        private string _languageCode;

        private Button _button;

        private readonly Signal _clickedSignal = new Signal();

        public string LanguageCode
        {
            get { return _languageCode; }
        }

        public Signal ButtonViewClickedSignal
        {
            get { return _clickedSignal; }
        }
        
        public string ButtonId
        {
            get { return _button.name; }
        }

        public void Highlight(bool highlight)
        {
            // NOTE: clarify with schuh how to do this
            /*
            var animator = GetComponent<Animator>();
            animator.SetAnim("highlight");
             */
        }

        private void Awake()
        {
            _button = GetComponent<Button>();

            _button.OnSingleClickAsObservable()
                .Subscribe(_ => _clickedSignal.Dispatch());
        }
    }
}