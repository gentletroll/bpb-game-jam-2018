using IoCPlus;
using UnityEngine.UI;

namespace General.Views
{
    public interface ISystemInfoView : IView
    {   
        Text Fps { get; }
        Text Memory { get; }
        Text Language { get; }    
    }
}