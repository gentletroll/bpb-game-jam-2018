using System;
using IoCPlus;
using UnityEngine;

namespace General.Views
{
    [RequireComponent(typeof(AudioSource))]
    public abstract class AudioSourceView : View, IAudioSourceView    
    {
        [SerializeField]
        string _audioCategory;

        private AudioSource _soundSource;

        public string AudioCategory 
        {
            get { return _audioCategory; }
        }

        public float Volume 
        { 
            get { return _soundSource.volume; }
            set { _soundSource.volume = value; }
        }

        void Awake()
        {
            _soundSource = GetComponent<AudioSource>();
        }

        public void Play(AudioClip audioClip)
        {
            if (audioClip == null)
                throw new Exception("Audio clip must not be null");

            _soundSource.clip = audioClip;                
            _soundSource.Play();
        }

        public void PlayDelayed(AudioClip audioClip, float seconds)
        {
            if (audioClip == null)
                throw new Exception("Audio clip must not be null");

            _soundSource.clip = audioClip;                
            _soundSource.PlayDelayed(seconds);
        }

        public void PlayOneShot(AudioClip audioClip)
        {
            if (audioClip == null)
                throw new Exception("Audio clip must not be null");

            _soundSource.PlayOneShot(audioClip);
        }
    }
}