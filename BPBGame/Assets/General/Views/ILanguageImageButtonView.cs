using IoCPlus;

namespace General.Views
{
    public interface ILanguageImageButtonView : IView    
    {
        void Highlight(bool highlight);

        string ButtonId { get; }
        string LanguageCode { get; }
                
        Signal ButtonViewClickedSignal { get; }
    }
}