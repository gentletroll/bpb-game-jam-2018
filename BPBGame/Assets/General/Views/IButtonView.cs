using IoCPlus;

namespace General.Views
{
    public interface IButtonView : IView
    {
        string ButtonId { get; }
        string ButtonText { get; set; }
        
        Signal ButtonViewClickedSignal { get; }
    }
}