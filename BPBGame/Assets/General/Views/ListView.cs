﻿using EnhancedUI;
using EnhancedUI.EnhancedScroller;
using IoCPlus;
using UnityEngine;

namespace General.Views
{
    public abstract class ListView<T> : View
    {        
        [SerializeField]
        private EnhancedScroller _scroller;

        protected SmallList<T> Data;

        protected abstract IEnhancedScrollerDelegate GetDelegate();

        void Awake()
        {
            Data = new SmallList<T>();

            _scroller.Delegate = GetDelegate();
        }

        public void Add(T data)
        {
            Data.Add(data);
        }

        public void Reload()
        {
            _scroller.ReloadData();                            
        }

        public void ClearAll()
        {
            Data.Clear();
            _scroller.ClearAll();                            
        }

        public void Refresh()
        {
            _scroller.RefreshActiveCellViews();
        }
        
        public virtual void SetBusy(bool busy) { }
    }
}