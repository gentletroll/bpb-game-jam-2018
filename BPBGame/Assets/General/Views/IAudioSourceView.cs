using IoCPlus;
using UnityEngine;

namespace General.Views
{
    public interface IAudioSourceView : IView
    {
        string AudioCategory { get; }

        float Volume { get; set; }

        void Play(AudioClip audioClip);
        void PlayDelayed(AudioClip audioClip, float seconds);
        
        void PlayOneShot(AudioClip audioClip);
    }
}