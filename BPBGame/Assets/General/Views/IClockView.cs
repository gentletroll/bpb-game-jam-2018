using IoCPlus;
using UnityEngine.UI;

namespace General.Views
{
    public interface IClockView : IView
    {
        Text TimeText { get; }
    }
}