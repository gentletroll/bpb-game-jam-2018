using IoCPlus;
using UnityEngine.UI;

namespace General.Views
{
    public interface ILanguageTextButtonView : IView
    {
        void Highlight(bool highlight);

        Text Text { get; }

        string ButtonId { get; }
        string LanguageCode { get; }
                
        Signal ButtonViewClickedSignal { get; }
    }
}