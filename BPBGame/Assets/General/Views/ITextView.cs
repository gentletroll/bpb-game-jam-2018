using IoCPlus;

namespace General.Views
{
    public interface ITextView : IView
    {
        string TextId { get; }
        string Text { get; set; }
    }
}