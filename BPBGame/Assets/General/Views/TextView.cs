using IoCPlus;
using UnityEngine;
using UnityEngine.UI;

namespace General.Views
{
    [RequireComponent(typeof(Text))]
    public class TextView : View, ITextView
    {
        private Text _text;

        public string TextId
        {
            get { return _text.name; }
        }

        public string Text
        {
            get { return _text.text; }
            set { _text.text = value; }
        }

        void Awake()
        {
            _text = GetComponent<Text>();
        }
    }
}