using IoCPlus;
using UnityEngine;
using UnityEngine.UI;

namespace General.Views
{
    public class SystemInfoView : View, ISystemInfoView
    {
        [SerializeField]
        private Text _fps;
        
        [SerializeField]
        private Text _memory;
        
        [SerializeField]
        private Text _language;

        public Text Fps
        {
            get { return _fps; }
        }

        public Text Memory
        {
            get { return _memory; }
        }

        public Text Language
        {
            get { return _language; }
        }
    }
}