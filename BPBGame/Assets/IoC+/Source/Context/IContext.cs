/// <copyright file="IContext.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using IoCPlus.Internal;
using System;
using System.Collections.Generic;

namespace IoCPlus {

    /// <summary>
    /// An IoC container that can nest contexts, set a context state and contain mediator instances.
    /// </summary>
    [ScriptDoc("Interfaces", "IContext", "An IoC container that can nest contexts, set a context state and contain mediator instances.")]
    public interface IContext {

        /// <summary>
        /// The parent context of this context. Is null when no parent is available.
        /// </summary>
        [ScriptDoc("The parent context of this context. Is null when no parent is available.")]
        IContext Parent { get; }

        /// <summary>
        /// Current context state of this context. Is null when no state is active.
        /// </summary>
        [ScriptDoc("Current context state of this context. Is null when no state is active.")]
        IContext State { get; }

        /// <summary>
        /// Contexts that are a child of this context.
        /// </summary>
        [ScriptDoc("Contexts that are a child of this context.")]
        IEnumerable<IContext> Children { get; }

        /// <summary>
        /// Mediator instances that belong to this context.
        /// </summary>
        [ScriptDoc("Mediator instances that belong to this context.")]
        IEnumerable<Mediator> Mediators { get; }

        /// <summary>
        /// Sets bindings and listens to signals for command executions. Optionally dispatches the EnterContextSignal.
        /// </summary>
        /// <param name="dispatchEnterSignal">Whether this context's EnterContextSignal needs to be dispatched.</param>
        [ScriptDoc("Sets bindings and listens to signals for command executions. Optionally dispatches the EnterContextSignal.")]
        void Initialize(bool dispatchEnterSignal = true);

        /// <summary>
        /// Removes this context. All child contexts and mediator instances will be moved to this context's parent.
        /// </summary>
        /// <param name="dispatchLeaveSignal">Whether this context's LeaveContextSignal needs to be dispatched.</param>
        [ScriptDoc("Removes this context. All child contexts and mediator instances will be moved to this context's parent.")]
        void Remove(bool dispatchLeaveSignal = true);

        /// <summary>
        /// Instantiates a context instance of the given type and assigns it as a child of this context.
        /// </summary>
        /// <param name="contextType">The type of context that needs to be instantiated.</param>
        /// <param name="dispatchEnterSignal">Whether the EnterContextSignal of the instantiated context needs to be dispatched.</param>
        /// <returns>The instantiated context instance.</returns>
        [ScriptDoc("Instantiates a context instance of the given type and assigns it as a child of this context.")]
        IContext InstantiateContext(Type contextType, bool dispatchEnterSignal = true);

        /// <summary>
        /// Instantiates a context instance of the given type and assigns it as a child of this context.
        /// </summary>
        /// <typeparam name="T">The type of context that needs to be instantiated.</typeparam>
        /// <param name="dispatchEnterSignal">Whether the EnterContextSignal of the instantiated context needs to be dispatched.</param>
        /// <returns>The instantiated context instance.</returns>
        IContext InstantiateContext<T>(bool dispatchEnterSignal = true) where T : Context, new();

        /// <summary>
        /// Sets the parent context of this context. This context will take over all injection and mediator bindings of parent but will overwrite them based on own bindings.
        /// </summary>
        /// <param name="parentContext">The context instance that will be set as parent.</param>
        [ScriptDoc("Sets the parent context of this context. This context will take over all injection and mediator bindings of the new parent but will overwrite them based on its own bindings.")]
        void SetParent(IContext parentContext);

        /// <summary>
        /// Switches this context with another context. All mediator instances, child contexts and the current context state will be passed to the new context type.
        /// </summary>
        /// <typeparam name="T">The type of context that needs to be switched to.</typeparam>
        /// <returns>The instantiated context that has been switched to.</returns>
        [ScriptDoc("Switches this context with another context. All mediator instances, child contexts and the current context state will be passed to the new context type.")]
        IContext Switch<T>() where T : Context;

        /// <summary>
        /// Switches this context with another context. All mediator instances, child contexts and the current context state will be passed to the new context type.
        /// </summary>
        /// <param name="contextType">The type of context that needs to be switched to.</param>
        /// <returns>The instantiated context that has been switched to.</returns>
        IContext Switch(Type contextType);

        /// <summary>
        /// Removes the current context state if any and instantiates a child context of the given type assigned as the new context state.
        /// </summary>
        /// <typeparam name="T">The type of context that will be the new state.</typeparam>
        [ScriptDoc("Removes the current context state if any and instantiates a child context of the given type assigned as the new context state.")]
        void SetContextState<T>() where T : Context, new();

        /// <summary>
        /// Removes the current context state if any and instantiates a child context of the given type assigned as the new context state.
        /// </summary>
        /// <param name="contextType">The type of context that will be the new state.</param>
        void SetContextState(Type contextType);

        /// <summary>
        /// Removes the current context state if any, parents the given context and assigs it as the new context state.
        /// </summary>
        /// <param name="state">The context instance that will be the new state.</param>
        void SetContextState(IContext state);

        /// <summary>
        /// Switches the current context state to the given context type. If no current state is available a new context will be instantiated.
        /// </summary>
        /// <typeparam name="T">The context type that the context state needs to switch to.</typeparam>
        [ScriptDoc("Switches the current context state to the given context type. If no current state is available a new context will be instantiated.")]
        void SwitchContextState<T>() where T : Context;

        /// <summary>
        /// Switches the current context state to the given context type. If no current state is available a new context will be instantiated.
        /// </summary>
        /// <param name="contextType">The context type that the context state needs to switch to.</param>
        void SwitchContextState(Type contextType);

        /// <summary>
        /// Instantiates an empty gameobject a component of the given view type bound to a new mediator instance based on this context and its parent's bindings. Binds mediator instance to this context.
        /// </summary>
        /// <typeparam name="T">The type of view that needs to be instantiated.</typeparam>
        /// <returns>The instantiated view.</returns>
        [ScriptDoc("Instantiates a view bound to a new mediator instance based on this context and its parent's bindings. Binds mediator instance to this context.")]
        T InstantiateView<T>() where T : View;

        /// <summary>
        /// Instantiates the given view type based on the given prefab, bound to a new mediator instance based on this context and its parent's bindings. Binds mediator instance to this context. Will also handle all nested view components in prefab.
        /// </summary>
        /// <typeparam name="T">The type of view that needs to be instantiated.</typeparam>
        /// <param name="prefab">The prefab that needs to be instantiated.</param>
        /// <returns>The instantiated view.</returns>
        T InstantiateView<T>(T prefab) where T : IView;

        /// <summary>
        /// Adds mediator instance to this context.
        /// </summary>
        /// <param name="mediator">The mediator instance that needs to be added.</param>
        [ScriptDoc("Adds mediator instance to this context.")]
        void AddMediator(Mediator mediator);

        // Test
        void AddView(View view);

        /// <summary>
        /// Returns all injection bindings in parents overwritten by the bindings defined in this context.
        /// </summary>
        /// <returns>The list of type/singleton bindings.</returns>
        [ScriptDoc("Returns all injection bindings in parents overwritten by the bindings defined in this context.")]
        Dictionary<Type, object> GetCulumativeInjectionBindings();

        /// <summary>
        /// Returns all labeled injection bindings in parents overwritten by the bindings defined in this context.
        /// </summary>
        /// <returns>The lists of type/singleton bindings, in a dictionary keyed by the binding labels.</returns>
        [ScriptDoc("Returns all labeled injection bindings in parents overwritten by the bindings defined in this context.")]
        Dictionary<string, Dictionary<Type, object>> GetCulumativeLabeledInjectionBindings();

        /// <summary>
        /// Returns all mediator bindings in parents overwritten by the bindings defined in this context.
        /// </summary>
        /// <returns>The list of view-type/mediator-type bindings.</returns>
        [ScriptDoc("Returns all mediator bindings in parents overwritten by the bindings defined in this context.")]
        Dictionary<Type, Type> GetCulumativeMediatorBindings();

    }

}