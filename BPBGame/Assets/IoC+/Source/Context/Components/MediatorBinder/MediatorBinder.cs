/// <copyright file="MediatorBinder.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using System;
using System.Collections.Generic;
using UnityEngine;

namespace IoCPlus.Internal {

    public class MediatorBinder : ContextComponent {

        public IEnumerable<KeyValuePair<Type, Type>> MediatorBindings { get { return mediatorBindings; } }
        public IEnumerable<Mediator> Mediators { get { return mediators; } }

        Dictionary<Type, Type> mediatorBindings = new Dictionary<Type, Type>();
        List<Mediator> mediators = new List<Mediator>();

        public void BindMediator<T, U>() where T : Mediator where U : View {
            mediatorBindings.Remove(typeof(U));
            mediatorBindings.Add(typeof(U), typeof(T));
        }

        public T InstantiateView<T>(T prefab) where T : IView {
            Dictionary<Type, Type> allMediatorBindings = GetCulumativeMediatorBindings();

            GameObject viewGameObject = null;
            
            if (prefab == null) {
                viewGameObject = new GameObject(typeof(T).Name);
                viewGameObject.AddComponent(typeof(T));
            } else if (prefab is View) {
                View prefabView = prefab as View;
                viewGameObject = UnityEngine.Object.Instantiate(prefabView.gameObject);
            } else {
                Debug.LogError("Can't instantiate view of type '" + typeof(T).Name + "' as it is not a view.");
                return default(T);
            }

            View[] viewsInChildren = viewGameObject.GetComponentsInChildren<View>(true);

            foreach (View view in viewsInChildren) {
                Type viewType = view.GetType();
                if (!allMediatorBindings.ContainsKey(viewType)) {
                    Debug.LogError("Can't process '" + viewType.Name + "' view as no context has bound a mediator to it.");
                    continue;
                }

                Type mediatorType = allMediatorBindings[viewType];
                InstantiateMediatorToView(view, mediatorType);
            }

            return viewGameObject.GetComponent<T>();
        }

        public void AddMediator(Mediator mediator) {
            IView view = mediator.GetView();
            Type viewType = view.GetType();
            Type mediatorType = mediator.GetType();

            Dictionary<Type, Type> allMediatorBindings = GetCulumativeMediatorBindings();
            if (allMediatorBindings.ContainsKey(viewType) &&
                allMediatorBindings[viewType] != mediatorType) {
                mediator.Remove();
                mediator = InstantiateMediatorToView(view, allMediatorBindings[viewType]);
                return;
            }

            mediator.OnRemoved += OnMediatorRemoval;

            ContextLogger.Log(MediatorAddLog.Create(context, mediator));
            mediators.Add(mediator);

            mediator.Dispose();

            Injector.Inject<Inject>(mediator, injectionBinder.GetCulumativeInjectionBindings(), Injector.CONTEXT_INJECTION_BINDING_MISSING_MESSAGE);
            Injector.Inject<Inject>(mediator, injectionBinder.GetCumulativeLabeledInjectionBindings(), true);

            mediator.Initialize();
        }

        public void AddView(View view) {
            Type viewType = view.GetType();

            Dictionary<Type, Type> allMediatorBindings = GetCulumativeMediatorBindings();
            InstantiateMediatorToView(view, allMediatorBindings[viewType]);
        }

        public void AssignAllMediatorsToParentOrRemove() {
            List<Mediator> mediatorsCopy = new List<Mediator>(mediators);
            while (mediators.Count > 0) {
                RemoveMediator(mediators[0]);
            }
            if (context.Parent != null) {
                mediatorsCopy.ForEach(x => context.Parent.AddMediator(x));
            }
        }

        public void RemoveMediator(Mediator mediator) {
            mediator.OnRemoved -= OnMediatorRemoval;

            ContextLogger.Log(MediatorRemoveLog.Create(context, mediator));
            mediators.Remove(mediator);
        }

        public Dictionary<Type, Type> GetCulumativeMediatorBindings() {
            if (context.Parent == null) { return mediatorBindings; }

            Dictionary<Type, Type> culumativeMediatorBindings = new Dictionary<Type, Type>(mediatorBindings);
            foreach (KeyValuePair<Type, Type> pair in context.Parent.GetCulumativeMediatorBindings()) {
                if (culumativeMediatorBindings.ContainsKey(pair.Key)) { continue; }
                culumativeMediatorBindings.Add(pair.Key, pair.Value);
            }

            return culumativeMediatorBindings;
        }

        public IEnumerable<Mediator> GetMediators() {
            return mediators;
        }

        /// <summary>
        /// Iterates through all mediators and updates their injections
        /// </summary>
        public void UpdateInjections() {
            Dictionary<Type, object> allInjectionBindings = injectionBinder.GetCulumativeInjectionBindings();
            Dictionary<string, Dictionary<Type, object>> allLabeledInjectionBindings = injectionBinder.GetCumulativeLabeledInjectionBindings();
            foreach (Mediator mediator in mediators) {
                mediator.Dispose();
                Injector.Inject<Inject>(mediator, allInjectionBindings, Injector.CONTEXT_INJECTION_BINDING_MISSING_MESSAGE);
                Injector.Inject<Inject>(mediator, allLabeledInjectionBindings, true);
                mediator.Initialize();
            }
        }

        private Mediator InstantiateMediatorToView(IView view, Type mediatorType) {
            Mediator mediator = Activator.CreateInstance(mediatorType) as Mediator;

            mediator.OnRemoved += OnMediatorRemoval;

            ContextLogger.Log(MediatorAddLog.Create(context, mediator));
            mediators.Add(mediator);

            Injector.Inject<Inject>(mediator, injectionBinder.GetCulumativeInjectionBindings(), Injector.CONTEXT_INJECTION_BINDING_MISSING_MESSAGE);
            Injector.Inject<Inject>(mediator, injectionBinder.GetCumulativeLabeledInjectionBindings(), true);

            mediator.SetView(view);
            mediator.Initialize();

            return mediator;
        }

        private void OnMediatorRemoval(Mediator mediator) {
            mediator.OnRemoved -= OnMediatorRemoval;

            ContextLogger.Log(MediatorRemoveLog.Create(context, mediator));
            mediators.Remove(mediator);
        }

    }

}