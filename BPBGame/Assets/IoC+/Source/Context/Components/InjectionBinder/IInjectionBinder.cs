/// <copyright file="IInjectionBinder.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using System;
using System.Collections.Generic;

namespace IoCPlus.Internal {

    public interface IInjectionBinder {
        T Bind<T>() where T : class, new();
        object Bind(Type keyType, object singleton);
        Dictionary<Type, object> GetCulumativeInjectionBindings();
        Dictionary<string, Dictionary<Type, object>> GetCumulativeLabeledInjectionBindings();
    }

}