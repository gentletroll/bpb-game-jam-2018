/// <copyright file="InjectionBinder.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using System.Collections.Generic;
using System;
using UnityEngine;

namespace IoCPlus.Internal {

    public class InjectionBinder : ContextComponent, IInjectionBinder {

        public IEnumerable<KeyValuePair<Type, object>> InjectionBindings { get { return injectionBindings; } }
        public IEnumerable<KeyValuePair<string, Dictionary<Type, object>>> LabeledInjectionBindings { get { return labeledInjectionBindings; } }

        readonly List<IContext> parents = new List<IContext>();

        Dictionary<Type, object> injectionBindings = new Dictionary<Type, object>();
        Dictionary<string, Dictionary<Type, object>> labeledInjectionBindings = new Dictionary<string, Dictionary<Type, object>>();

        Dictionary<Type, object> cumulativeInjectionBindings;
        Dictionary<string, Dictionary<Type, object>> cumulativeLabeledInjectionBindings;

        bool isDirty = true;

        public T Bind<T>() where T : class, new() {
            return Bind<T, T>();
        }

        public T Bind<T, U>() where T : class where U : T, new() {
            return Bind<T>(new U());
        }

        public T Bind<T>(object singleton) where T : class {
            isDirty = true;

            Type keyType = typeof(T);
            Type instanceType = singleton.GetType();

            if (!instanceType.IsSubclassOf(keyType) &&
                !keyType.IsAssignableFrom(instanceType) &&
                instanceType != keyType) {
                Debug.Log("Can't bind instance of type '" + instanceType.Name + "' to '" + keyType.Name + "' because it does not subclass, implement its interface or match the type.");
                return null;
            }

            Type signalType = typeof(AbstractSignal);
            if (instanceType.IsSubclassOf(signalType) ||
                signalType.IsAssignableFrom(instanceType) ||
                signalType == instanceType) {
                (singleton as AbstractSignal).MarkAsInjected();
            }

            injectionBindings.Remove(keyType);
            injectionBindings.Add(keyType, singleton);
            return singleton as T;
        }

        public object Bind(Type keyType, object singleton) {
            isDirty = true;

            injectionBindings.Remove(keyType);
            injectionBindings.Add(keyType, singleton);
            return singleton;
        }

        public T BindLabeled<T>(object label) where T : class, new() {
            return BindLabeled<T, T>(label);
        }

        public T BindLabeled<T, U>(object label) where T : class where U : T, new() {
            return BindLabeled<T>(new U(), label);
        }

        public T BindLabeled<T>(object singleton, object label) where T : class {
            isDirty = true;

            Type keyType = typeof(T);
            Type instanceType = singleton.GetType();

            if (!instanceType.IsSubclassOf(keyType) &&
                !keyType.IsAssignableFrom(instanceType) &&
                instanceType != keyType) {
                Debug.Log("Can't bind instance of type '" + instanceType.Name + "' to '" + keyType.Name + "' because it does not subclass, implement its interface or match the type.");
                return null;
            }

            string labelString = label.GetType() == typeof(string) ? label as string : label.ToString();
            if (!labeledInjectionBindings.ContainsKey(labelString)) {
                labeledInjectionBindings.Add(labelString, new Dictionary<Type, object>());
            }

            Dictionary<Type, object> labeledBindings = labeledInjectionBindings[labelString];
            labeledBindings.Remove(keyType);
            labeledBindings.Add(keyType, singleton);
            return singleton as T;
        }

        public Dictionary<Type, object> GetCulumativeInjectionBindings() {
            if (IsDirty()) {
                UpdateCumulativeBindings();
                Clean();
            }
            return cumulativeInjectionBindings;
        }

        public Dictionary<string, Dictionary<Type, object>> GetCumulativeLabeledInjectionBindings() {
            if (IsDirty()) {
                UpdateCumulativeBindings();
                Clean();
            }
            return cumulativeLabeledInjectionBindings;
        }

        private void UpdateCumulativeBindings() {
            UpdateCumulativeInjectionsBindings();
            UpdateCumulativeLabeledInjectionBindings();
        }

        private void UpdateCumulativeInjectionsBindings() {
            cumulativeInjectionBindings = new Dictionary<Type, object>(injectionBindings);

            if (context.Parent == null) { return; }

            foreach (KeyValuePair<Type, object> pair in context.Parent.GetCulumativeInjectionBindings()) {
                if (cumulativeInjectionBindings.ContainsKey(pair.Key)) { continue; }
                cumulativeInjectionBindings.Add(pair.Key, pair.Value);
            }
        }

        private void UpdateCumulativeLabeledInjectionBindings() {
            cumulativeLabeledInjectionBindings = new Dictionary<string, Dictionary<Type, object>>();

            if (context.Parent != null) {
                cumulativeLabeledInjectionBindings = context.Parent.GetCulumativeLabeledInjectionBindings();
            }

            foreach (KeyValuePair<string, Dictionary<Type, object>> labeledBindings in labeledInjectionBindings) {
                string label = labeledBindings.Key;
                if (!cumulativeLabeledInjectionBindings.ContainsKey(label)) {
                    cumulativeLabeledInjectionBindings.Add(label, new Dictionary<Type, object>(labeledBindings.Value));
                    continue;
                }
                foreach (KeyValuePair<Type, object> binding in labeledBindings.Value) {
                    if (cumulativeLabeledInjectionBindings[label].ContainsKey(binding.Key)) {
                        cumulativeLabeledInjectionBindings[label][binding.Key] = binding.Value;
                    } else {
                        cumulativeLabeledInjectionBindings[label].Add(binding.Key, binding.Value);
                    }
                }
            }
        }

        private bool IsDirty() {
            if (isDirty) { return true; }
            int index = 0;
            IContext parent = context.Parent;
            while (parent != null) {
                if (parents.Count <= index) { return true; }
                if (parents[index] != parent) { return true; }
                parent = parent.Parent;
                index++;
            }
            return false;
        }

        private void Clean() {
            parents.Clear();
            IContext parent = context.Parent;
            while (parent != null) {
                parents.Add(parent);
                parent = parent.Parent;
            }
            isDirty = false;
        }

    }

}