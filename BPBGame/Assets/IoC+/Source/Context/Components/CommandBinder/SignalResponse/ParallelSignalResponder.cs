/// <copyright file="ParallelSignalResponder.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using System.Collections.Generic;

namespace IoCPlus.Internal {

    public class ParallelSignalResponder : SignalResponder {

        bool isAborted;
        List<AbstractCommand> releasedCommands;
        List<AbstractCommand> revertedCommands;
        AbstractCommand abortedCommand;

        public ParallelSignalResponder(SignalResponse signalResponse, int responseIndex) : base(signalResponse, responseIndex) { }

        public override void Respond(Context context, AbstractSignal signal) {
            base.Respond(context, signal);

            isAborted = false;
            releasedCommands = new List<AbstractCommand>();
            revertedCommands = new List<AbstractCommand>();
            abortedCommand = null;

            for (int i = 0; i < signalResponse.Commands.Count; i++) {
                signalResponse.Commands[i].OnRelease = OnCommandReleased;
                signalResponse.Commands[i].OnAbort = OnCommandAborted;
            }

            for (int i = 0; i < signalResponse.Commands.Count; i++) {
                if (isAborted) { break; }
                ContextLogger.Log(CommandExecuteLog.Create(context, signal, responseIndex, i));
                signalResponse.Commands[i].PerformExecution();
            }
        }

        private void OnCommandReleased(AbstractCommand command) {
            int commandIndex = signalResponse.Commands.IndexOf(command);

            if (isAborted) {
                bool isAlreadyReverted = revertedCommands.Contains(command);
                if (signalResponse.RevertOnAbort && !isAlreadyReverted && command != abortedCommand) {
                    revertedCommands.Add(command);
                    ContextLogger.Log(CommandRevertLog.Create(context, signal, responseIndex, commandIndex));
                    command.PerformRevertion();
                }
                return;
            }

            releasedCommands.Add(command);
            ContextLogger.Log(CommandReleaseLog.Create(context, signal, responseIndex, commandIndex));

            if (releasedCommands.Count == signalResponse.Commands.Count) {
                if (signalResponse.FinishCommand != null) {
                    ContextLogger.Log(FinishCommandExecuteLog.Create(context, signal, responseIndex));
                    signalResponse.FinishCommand.PerformExecution();
                }
            }
        }

        private void OnCommandAborted(AbstractCommand command) {
            isAborted = true;
            abortedCommand = command;

            int commandIndex = signalResponse.Commands.IndexOf(command);
            ContextLogger.Log(CommandAbortLog.Create(context, signal, signalResponse.Index, commandIndex));

            if (signalResponse.AbortCommand != null) {
                ContextLogger.Log(AbortCommandExecuteLog.Create(context, signal, signalResponse.Index));
                signalResponse.AbortCommand.PerformExecution();
            }

            if (signalResponse.RevertOnAbort) {
                foreach (Command releasedCommand in releasedCommands) {
                    revertedCommands.Add(releasedCommand);
                    int releasedCommandIndex = releasedCommands.IndexOf(releasedCommand);
                    ContextLogger.Log(CommandRevertLog.Create(context, signal, responseIndex, releasedCommandIndex));
                    releasedCommand.PerformRevertion();
                }
            }
        }

    }

}
