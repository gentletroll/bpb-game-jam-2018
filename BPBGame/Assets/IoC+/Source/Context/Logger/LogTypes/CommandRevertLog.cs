/// <copyright file="CommandRevertLog.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus.Internal {

    public class CommandRevertLog : ContextLog {
        public Context Context { get; private set; }
        public AbstractSignal Signal { get; private set; }
        public int ResponseIndex { get; private set; }
        public int CommandIndex { get; private set; }

        public static CommandRevertLog Create(Context context, AbstractSignal signal, int responseIndex, int commandIndex) {
            if (!ShouldLog()) { return null; }
            CommandRevertLog log = Pool<CommandRevertLog>.Create();
            log.Context = context;
            log.Signal = signal;
            log.CommandIndex = commandIndex;
            return log;
        }

        public override void Trigger() {
            if (ContextLogger.OnCommandRevert == null) { return; }
            ContextLogger.OnCommandRevert(Context, Signal, ResponseIndex, CommandIndex);
            Pool<CommandRevertLog>.Retire(this);
        }
    }

}