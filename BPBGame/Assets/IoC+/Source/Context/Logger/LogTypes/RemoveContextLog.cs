/// <copyright file="RemoveContextLog.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus.Internal {

    public class RemoveContextLog : ContextLog {
        public Context Context { get; private set; }

        public static RemoveContextLog Create(Context context) {
            if (!ShouldLog()) { return null; }
            RemoveContextLog log = Pool<RemoveContextLog>.Create();
            log.Context = context;
            return log;
        }

        public override bool HasProcessInterval() { return false; }

        public override void Trigger() {
            if (ContextLogger.OnContextRemoved == null) { return; }
            ContextLogger.OnContextRemoved(Context);
            Pool<RemoveContextLog>.Retire(this);
        }
    }

}