/// <copyright file="InitializedContextLog.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus.Internal {

    public class InitializedContextLog : ContextLog {
        public Context Context { get; private set; }

        public static InitializedContextLog Create(Context context) {
            if (!ShouldLog()) { return null; }
            InitializedContextLog log = Pool<InitializedContextLog>.Create();
            log.Context = context;
            return log;
        }

        public override bool HasProcessInterval() { return false; }

        public override void Trigger() {
            if (ContextLogger.OnContextInitialized == null) { return; }
            ContextLogger.OnContextInitialized(Context);
            Pool<InitializedContextLog>.Retire(this);
        }
    }

}