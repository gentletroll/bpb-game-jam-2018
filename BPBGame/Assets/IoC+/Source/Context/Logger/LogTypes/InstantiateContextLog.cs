/// <copyright file="InstantiateContextLog.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus.Internal {

    public class InstantiateContextLog : ContextLog {
        public Context Context { get; private set; }

        public static InstantiateContextLog Create(Context context) {
            if (!ShouldLog()) { return null; }
            InstantiateContextLog log = Pool<InstantiateContextLog>.Create();
            log.Context = context;
            return log;
        }

        public override bool HasProcessInterval() { return false; }

        public override void Trigger() {
            if (ContextLogger.OnContextInstantiated == null) { return; }
            ContextLogger.OnContextInstantiated(Context);
            Pool<InstantiateContextLog>.Retire(this);
        }
    }

}