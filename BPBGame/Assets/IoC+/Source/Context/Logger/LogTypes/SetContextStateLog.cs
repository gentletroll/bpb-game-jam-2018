/// <copyright file="SetContextStateLog.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus.Internal {

    public class SetContextStateLog : ContextLog {
        public Context Context { get; private set; }
        public IContext StateContext { get; private set; }

        public static SetContextStateLog Create(Context context, IContext stateContext) {
            if (!ShouldLog()) { return null; }
            SetContextStateLog log = Pool<SetContextStateLog>.Create();
            log.Context = context;
            log.StateContext = stateContext;
            return log;
        }

        public override bool HasProcessInterval() { return false; }

        public override void Trigger() {
            if (ContextLogger.OnContextStateSet == null) { return; }
            ContextLogger.OnContextStateSet(Context, StateContext);
            Pool<SetContextStateLog>.Retire(this);
        }
    }

}