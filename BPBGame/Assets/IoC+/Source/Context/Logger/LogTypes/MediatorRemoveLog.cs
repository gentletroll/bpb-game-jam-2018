/// <copyright file="MediatorRemoveLog.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus.Internal {

    public class MediatorRemoveLog : ContextLog {
        public Context Context { get; private set; }
        public Mediator Mediator { get; private set; }

        public static MediatorRemoveLog Create(Context context, Mediator mediator) {
            if (!ShouldLog()) { return null; }
            MediatorRemoveLog log = Pool<MediatorRemoveLog>.Create();
            log.Context = context;
            log.Mediator = mediator;
            return log;
        }
        public override bool HasProcessInterval() { return false; }

        public override void Trigger() {
            if (ContextLogger.OnMediatorRemove == null) { return; }
            ContextLogger.OnMediatorRemove(Context, Mediator);
            Pool<MediatorRemoveLog>.Retire(this);
        }
    }

}