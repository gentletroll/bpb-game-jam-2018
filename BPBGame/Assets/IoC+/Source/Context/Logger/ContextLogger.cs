/// <copyright file="ContextLogger.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using System;
using System.Collections.Generic;
using UnityEngine;

namespace IoCPlus.Internal {

    public static class ContextLogger {

        public const int MAX_LOG_AMOUNT = 999;

        public static int LogCountWithInterval { get; private set; }
        public static int LogCount { get { return logs.Count; } }

        public static Action<Context> OnContextInstantiated;
        public static Action<Context> OnContextInitialized;
        public static Action<Context> OnContextRemoved;
        public static Action<Context, IContext> OnContextParentSet;
        public static Action<Context, IContext> OnContextStateSet;
        public static Action<AbstractSignal> OnSignalDispatch;
        public static Action<Context, AbstractSignal, int, int> OnCommandExecute;
        public static Action<Context, AbstractSignal, int, int> OnCommandRelease;
        public static Action<Context, AbstractSignal, int, int> OnCommandAbort;
        public static Action<Context, AbstractSignal, int, int> OnCommandRevert;
        public static Action<Context, AbstractSignal, int> OnAbortCommandExecute;
        public static Action<Context, AbstractSignal, int> OnAbortCommandRelease;
        public static Action<Context, AbstractSignal, int> OnFinishCommandExecute;
        public static Action<Context, AbstractSignal, int> OnFinishCommandRelease;
        public static Action<Context, Mediator> OnMediatorAdd;
        public static Action<Context, Mediator> OnMediatorRemove;

        private const float LOG_CALLBACK_INTERVAL = .2f;

        private static Queue<ContextLog> logs = new Queue<ContextLog>();
        private static float timeSinceLastLogCallback;
        private static bool lastLogHadReaction;

        public static void ResetActions() {
            OnContextInstantiated = null;
            OnContextInitialized = null;
            OnContextRemoved = null;
            OnContextParentSet = null;
            OnContextStateSet = null;
            OnSignalDispatch = null;
            OnCommandExecute = null;
            OnCommandRelease = null;
            OnCommandAbort = null;
            OnCommandRevert = null;
            OnAbortCommandExecute = null;
            OnAbortCommandRelease = null;
            OnFinishCommandExecute = null;
            OnFinishCommandRelease = null;
            OnMediatorAdd = null;
            OnMediatorRemove = null;
        }

        public static void Log<T>(T log) where T : ContextLog {
            if (!Application.isEditor) { return; }
            if (!Application.isPlaying) { return; }
            if (log == null) { return; }
            logs.Enqueue(log);
            LogCountWithInterval += log.HasProcessInterval() ? 1 : 0;
        }

        public static void NotifyLogReaction() {
            lastLogHadReaction = true;
        }

        public static void Update() {
            if (!Application.isEditor) { return; }
            timeSinceLastLogCallback += Time.deltaTime;
            while (ShouldDispatchNextLog()) {
                lastLogHadReaction = false;
                ContextLog dispatchedLog = DispatchLog();
                if (dispatchedLog.HasProcessInterval() && lastLogHadReaction) {
                    timeSinceLastLogCallback = 0.0f;
                }
            }
        }

        public static void Step() {
            if (!Application.isEditor) { return; }
            while (logs.Count > 0) {
                lastLogHadReaction = false;
                ContextLog dispatchedLog = DispatchLog();
                if (dispatchedLog.HasProcessInterval() && lastLogHadReaction) {
                    timeSinceLastLogCallback = 0.0f;
                    return;
                }
            }
        }

        private static ContextLog DispatchLog() {
            if (logs.Count == 0) { return null; }
            ContextLog log = logs.Dequeue();
            LogCountWithInterval -= log.HasProcessInterval() ? 1 : 0;
            log.Trigger();
            return log;
        }

        private static bool ShouldDispatchNextLog() {
            if (logs.Count == 0) { return false; }
            if (LogCountWithInterval > MAX_LOG_AMOUNT) { return true; }

            switch (MonitorSettings.TimeControlSetting) {
                case MonitorSettings.TimeControl.Paused:
                    return false;
                case MonitorSettings.TimeControl.Delayed:
                    return timeSinceLastLogCallback >= LOG_CALLBACK_INTERVAL;
                case MonitorSettings.TimeControl.Synced:
                    timeSinceLastLogCallback = LOG_CALLBACK_INTERVAL;
                    return true;
            }

            return true;
        }

    }

}