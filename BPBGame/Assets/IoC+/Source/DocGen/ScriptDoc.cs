/// <copyright file="ScriptDoc.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using System;

namespace IoCPlus.Internal {

    public class ScriptDoc : Attribute {
        public string Title { get; private set; }
        public string Summary { get; private set; }
        public string ShortSummary { get; private set; }
        public string Category { get; private set; }

        public ScriptDoc() { }
        public ScriptDoc(string summary) {
            Summary = summary;
        }
        public ScriptDoc(string category, string title, string summary) : this (category, title, summary, summary) { }
        public ScriptDoc(string category, string title, string shortSummary, string summary) {
            Category = category;
            Title = title;
            ShortSummary = shortSummary;
            Summary = summary;
        }
    }

}