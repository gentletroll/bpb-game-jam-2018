/// <copyright file="Mediator.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using System;
using UnityEngine;
using IoCPlus.Internal;

namespace IoCPlus.Internal {
    
    public abstract class Mediator {

        public Action<Mediator> OnRemoved;

        public abstract void SetView(IView view);

        /// <summary>
        /// Use this method to add listeners to signals of context injections and view, and to initialize the view.
        /// </summary>
        [ScriptDoc("Override this method to add listeners to signals of context injections and view, and to initialize the view.")]
        public abstract void Initialize();

        /// <summary>
        /// Use this method to remove listeners from signals of context injections and view.
        /// </summary>
        [ScriptDoc("Override this method to remove listeners from signals of context injections and view.")]
        public abstract void Dispose();

        public abstract void Remove();
        public abstract IView GetView();

    }

}

namespace IoCPlus {

    /// <summary>
    /// Takes control over and instance given view type. Gets removed on view's destruction. Is instantiated and bound to the view based on the context's mediator bindings. Use the inject attribute on fields to inject its value via the context's injection bindings.
    /// </summary>
    [ScriptDoc("Classes", "Mediator", "Takes control over a view instance of the given type.", "Takes control over a view instance of the given type. Is removed on view's destruction. Is instantiated and bound to the view based on the context's mediator bindings. Use the inject attribute on fields to inject its value via the context's injection bindings.")]
    public abstract class Mediator<T> : Mediator where T : class, IView {

        /// <summary>
        /// The view instance this mediator is controlling.
        /// </summary>
        [ScriptDoc("The view instance this mediator is controlling.")]
        protected T view { get; private set; }

        public override sealed void SetView(IView view) {
            Type mediatorType = GetType();
            Type viewType = view.GetType();
            Type neededViewType = typeof(T);

            bool interfaceNeeded = neededViewType.IsInterface;
            bool implementsInterface = neededViewType.IsAssignableFrom(viewType);
            bool isSubclass = viewType.IsSubclassOf(neededViewType);

            if (viewType != neededViewType &&
                ((interfaceNeeded && !implementsInterface) ||
                 (!interfaceNeeded && !isSubclass))) {
                Debug.LogError("Can't assign view '" + viewType.Name + "' to mediator '" + mediatorType.Name + "' because it needs a '" + neededViewType.Name + "' or a type derived from a a '" + neededViewType.Name + "'!");
                return;
            }

            this.view = view as T;
            this.view.DeleteSignal.AddListener(OnViewDeleteSignal);
        }

        public override sealed void Remove() {
            view.DeleteSignal.RemoveListener(OnViewDeleteSignal);
            Dispose();
            if (OnRemoved != null) {
                OnRemoved(this);
            }
            OnRemove();
        }

        public override IView GetView() {
            return view;
        }

        /// <summary>
        /// Called when this mediator is removed and is no longer needed.
        /// </summary>
        [ScriptDoc("Called when this mediator is removed and is no longer needed.")]
        protected virtual void OnRemove() { }

        /// <summary>
        /// Destroys the view.
        /// </summary>
        [ScriptDoc("Destroys the view.")]
        protected void DestroyView() {
            view.Destroy();
        }

        private void OnViewDeleteSignal() {
            Remove();
        }

    }

}