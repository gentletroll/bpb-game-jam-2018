/// <copyright file="InstantiateViewCommand.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus {

    public class InstantiateViewCommand<T> : Command where T : View {

        [Inject] IContext context;

        protected override void Execute() {
            context.InstantiateView<T>();
        }

    }

}
