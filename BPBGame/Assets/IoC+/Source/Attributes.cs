/// <copyright file="Attributes.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using IoCPlus.Internal;
using System;

namespace IoCPlus {

    /// <summary>
    /// The value of fields with this attribute will be injected by the context based on the field type. Use an object as label to inject different values based on the same type.
    /// </summary>
    [ScriptDoc("Attributes", "Inject", "Injects a value based on context bindings.", "The value of fields with this attribute will be injected by the context based on the field type. Use this attribute only in commands and mediators.")]
    public class Inject : Attribute {
        public readonly bool HasLabel;
        public readonly string Label;
        public Inject() { }
        public Inject(object label) {
            Label = label.ToString();
            HasLabel = true;
        }
    }

    /// <summary>
    /// The value of fields with this attribute will be injected by the signal parameters of that type. Use this attribute only in commands.
    /// </summary>
    [ScriptDoc("Attributes", "InjectParameter", "Injects a value based on signal parameters.", "The value of fields with this attribute will be injected by the signal parameters of that type. Use this attribute only in commands.")]
    public class InjectParameter : Attribute { }

    /// <summary>
    /// Add this attribute to fields with the Inject or InjectParameter attribute assigned to suppress error logs that are thrown when no injection binding is set via a context or signal parameter.
    /// </summary>
    [ScriptDoc("Attributes", "Optional", "Suppresses error logs on missing injection bindings.", "Add this attribute to fields with the Inject or InjectParameter attribute assigned to suppress error logs that are thrown when no injection binding is set via a context or signal parameter.")]
    public class Optional : Attribute { }

    /// <summary>
    /// Contexts with this attribute will be listed higher up in the IoC+ Monitor, based on the given priority value.
    /// </summary>
    [ScriptDoc("Attributes", "Prioritize", "Prioritizes a context in the IoC+ Monitor", "Contexts with this attribute will be listed higher up in the IoC+ Monitor, based on the given priority value.")]
    public class Prioritize : Attribute {
        public readonly int Priority;
        public Prioritize(int priority) {
            Priority = priority;
        }
        public static int Compare(object a, object b) {
            Prioritize aPrioritize = a.GetType().GetAttribute<Prioritize>();
            Prioritize bPrioritize = b.GetType().GetAttribute<Prioritize>();
            int aPriority = aPrioritize == null ? 0 : aPrioritize.Priority;
            int bPriority = bPrioritize == null ? 0 : bPrioritize.Priority;
            return bPriority.CompareTo(aPriority);
        }
    }

    namespace Internal {

        public static class AttributeHelper {

            public static T GetAttribute<T>(this Type type) where T : Attribute {
                object[] attributes = type.GetCustomAttributes(typeof(Prioritize), true);
                if (attributes.Length > 0) {
                    return attributes[0] as T;
                }
                return null;
            }

        }

    }

}