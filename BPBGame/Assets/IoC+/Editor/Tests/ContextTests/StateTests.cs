/// <copyright file="StateTests.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using NUnit.Framework;
using NSubstitute;
using System.Linq;

namespace IoCPlus.ContextTests {

    public class StateTests {

        private class TestTypes {
            public class TestContext : Context { }
            public class TestStateContext : Context { }
            public class AlternativeTestStateContext : Context { }
            public class TestChildState : Context { }
        }

        [Test]
        public void ContextHasNoState_OnInitialize() {
            //Arrange
            Context context = new TestTypes.TestContext();

            //Act
            context.Initialize();

            //Assert
            Assert.That(context.State == null);
        }

        [Test]
        public void ContextSetsState_OnSetState() {
            //Arrange
            Context context = new TestTypes.TestContext();
            context.Initialize();

            //Act
            context.SetContextState<TestTypes.TestStateContext>();

            //Assert
            Assert.That(context.State != null);
            Assert.That(context.State.GetType() == typeof(TestTypes.TestStateContext));
        }

        [Test]
        public void ContextStateIsChildState_OnSetState() {
            //Arrange
            Context context = new TestTypes.TestContext();
            context.Initialize();

            //Act
            context.SetContextState<TestTypes.TestStateContext>();

            //Assert
            Assert.That(context.State != null);
            Assert.That(context.Children.Contains(context.State));
        }

        [Test]
        public void ContextRemovesOldState_OnSetState() {
            //Arrange
            Context context = new TestTypes.TestContext();
            context.Initialize();

            context.SetContextState<TestTypes.TestStateContext>();

            //Act
            context.SetContextState<TestTypes.AlternativeTestStateContext>();

            //Assert
            Assert.That(context.State != null);
            Assert.That(context.Children.Count() == 1);
            Assert.That(context.Children.ElementAt(0).GetType() == typeof(TestTypes.AlternativeTestStateContext));
        }

        [Test]
        public void ContextKeepsOtherChildren_OnSetState() {
            //Arrange
            Context context = new TestTypes.TestContext();
            context.Initialize();

            context.InstantiateContext<TestTypes.TestChildState>();

            //Act
            context.SetContextState<TestTypes.TestStateContext>();

            //Assert
            Assert.That(context.State != null);
            Assert.That(context.Children.Count() == 2);
            Assert.That(context.Children.First(x => x.GetType() == typeof(TestTypes.TestStateContext)) != null);
            Assert.That(context.Children.First(x => x.GetType() == typeof(TestTypes.TestChildState)) != null);
        }

        [Test]
        public void ContextDispatchesEnterContextSignalOfState_OnSetState() {
            //Arrange
            Context context = new TestTypes.TestContext();
            context.Initialize();

            //Act
            context.SetContextState<TestTypes.TestStateContext>();

            //Assert
            Assert.That(context.State != null);

            object enterContextSignal;
            context.State.GetCulumativeInjectionBindings().TryGetValue(typeof(EnterContextSignal), out enterContextSignal);
            Assert.That(enterContextSignal != null);
            
            Assert.That((enterContextSignal as EnterContextSignal).DispatchCount == 1);
        }

        [Test]
        public void ContextDispatchesLeaveContextSignalOfOldState_OnSetState() {
            //Arrange
            Context context = new TestTypes.TestContext();
            context.Initialize();
            context.SetContextState<TestTypes.TestStateContext>();

            IContext oldState = context.State;
            Assert.That(oldState != null);

            object leaveContextSignal;
            oldState.GetCulumativeInjectionBindings().TryGetValue(typeof(LeaveContextSignal), out leaveContextSignal);

            //Assert
            Assert.That(leaveContextSignal != null);
            Assert.That((leaveContextSignal as LeaveContextSignal).DispatchCount == 0);

            //Act
            context.SetContextState<TestTypes.AlternativeTestStateContext>();

            //Assert
            Assert.That((leaveContextSignal as LeaveContextSignal).DispatchCount == 1);
        }

        [Test]
        public void ContextSetsState_OnSwitchState() {
            //Arrange
            Context context = new TestTypes.TestContext();
            context.Initialize();

            //Act
            context.SwitchContextState<TestTypes.TestStateContext>();

            //Assert
            Assert.That(context.State != null);
            Assert.That(context.State.GetType() == typeof(TestTypes.TestStateContext));
        }

        [Test]
        public void ContextStateIsChildState_OnSwitchState() {
            //Arrange
            Context context = new TestTypes.TestContext();
            context.Initialize();

            //Act
            context.SwitchContextState<TestTypes.TestStateContext>();

            //Assert
            Assert.That(context.State != null);
            Assert.That(context.Children.Contains(context.State));
        }

        [Test]
        public void ContextRemovesOldState_OnSwitchState() {
            //Arrange
            Context context = new TestTypes.TestContext();
            context.Initialize();

            context.SwitchContextState<TestTypes.TestStateContext>();

            //Act
            context.SwitchContextState<TestTypes.AlternativeTestStateContext>();

            //Assert
            Assert.That(context.State != null);
            Assert.That(context.Children.Count() == 1);
            Assert.That(context.Children.ElementAt(0).GetType() == typeof(TestTypes.AlternativeTestStateContext));
        }

        [Test]
        public void ContextKeepsOtherChildren_OnSwitchState() {
            //Arrange
            Context context = new TestTypes.TestContext();
            context.Initialize();

            context.InstantiateContext<TestTypes.TestChildState>();

            //Act
            context.SwitchContextState<TestTypes.TestStateContext>();

            //Assert
            Assert.That(context.State != null);
            Assert.That(context.Children.Count() == 2);
            Assert.That(context.Children.First(x => x.GetType() == typeof(TestTypes.TestStateContext)) != null);
            Assert.That(context.Children.First(x => x.GetType() == typeof(TestTypes.TestChildState)) != null);
        }

        [Test]
        public void ContextDispatchesEnterContextSignalOfFirstState_OnSwitchState() {
            //Arrange
            Context context = new TestTypes.TestContext();
            context.Initialize();

            //Act
            context.SwitchContextState<TestTypes.TestStateContext>();

            //Assert
            Assert.That(context.State != null);

            object enterContextSignal;
            context.State.GetCulumativeInjectionBindings().TryGetValue(typeof(EnterContextSignal), out enterContextSignal);
            Assert.That(enterContextSignal != null);

            Assert.That((enterContextSignal as EnterContextSignal).DispatchCount == 1);
        }

        [Test]
        public void ContextDoesNotDispatchEnterContextSignalOfSecondState_OnSwitchState() {
            //Arrange
            Context context = new TestTypes.TestContext();
            context.Initialize();
            context.SwitchContextState<TestTypes.TestStateContext>();

            //Act
            context.SwitchContextState<TestTypes.AlternativeTestStateContext>();

            //Assert
            Assert.That(context.State != null);

            object enterContextSignal;
            context.State.GetCulumativeInjectionBindings().TryGetValue(typeof(EnterContextSignal), out enterContextSignal);
            Assert.That(enterContextSignal != null);

            Assert.That((enterContextSignal as EnterContextSignal).DispatchCount == 0);
        }

        [Test]
        public void ContextDoesNotDispatchLeaveContextSignalOfOldState_OnSwitchState() {
            //Arrange
            Context context = new TestTypes.TestContext();
            context.Initialize();
            context.SetContextState<TestTypes.TestStateContext>();

            IContext oldState = context.State;
            Assert.That(oldState != null);

            object leaveContextSignal;
            oldState.GetCulumativeInjectionBindings().TryGetValue(typeof(LeaveContextSignal), out leaveContextSignal);

            //Assert
            Assert.That(leaveContextSignal != null);
            Assert.That((leaveContextSignal as LeaveContextSignal).DispatchCount == 0);

            //Act
            context.SwitchContextState<TestTypes.AlternativeTestStateContext>();

            //Assert
            Assert.That((leaveContextSignal as LeaveContextSignal).DispatchCount == 0);
        }

    }

}