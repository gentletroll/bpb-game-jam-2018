/// <copyright file="CommandBindingTests.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using IoCPlus.Internal;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IoCPlus.ContextTests {

    public class CommandBindingTests {

        private class TestContext : Context { }
        private class TestTypes {
            public class TestCommand : Command {
                public bool IsExecuted { get; private set; }
                protected override void Execute() {
                    IsExecuted = true;
                }
            }
            public class OneExecuteParameterCommand : Command<string> {
                public string Parameter1 { get; private set; }
                protected override void Execute(string parameter1) {
                    Parameter1 = parameter1;
                }
                protected override void Revert(string parameter1) {
                    Parameter1 = parameter1;
                }
            }
            public class TwoExecuteParametersCommand : Command<string, int> {
                public string Parameter1 { get; private set; }
                public int Parameter2 { get; private set; }
                protected override void Execute(string parameter1, int parameter2) {
                    Parameter1 = parameter1;
                    Parameter2 = parameter2;
                }
                protected override void Revert(string parameter1, int parameter2) {
                    Parameter1 = parameter1;
                    Parameter2 = parameter2;
                }
            }
            public class ThreeExecuteParametersCommand : Command<string, int, Command> {
                public string Parameter1 { get; private set; }
                public int Parameter2 { get; private set; }
                public Command Parameter3 { get; private set; }
                protected override void Execute(string parameter1, int parameter2, Command parameter3) {
                    Parameter1 = parameter1;
                    Parameter2 = parameter2;
                    Parameter3 = parameter3;
                }
                protected override void Revert(string parameter1, int parameter2, Command parameter3) {
                    Parameter1 = parameter1;
                    Parameter2 = parameter2;
                    Parameter3 = parameter3;
                }
            }
            public class TestSignal : Signal { }
            public class TestContext : Context {
                protected override void SetBindings() {
                    base.SetBindings();
                    Bind<TestSignal>();
                    On<TestSignal>().Do<TestCommand>();
                }
            }
            public class GotoStateTestContext : Context {
                protected override void SetBindings() {
                    base.SetBindings();
                    Bind<TestSignal>();
                    On<TestSignal>().GotoState<TestContext>();
                }
            }
            public class SwitchContextTestContext : Context {
                protected override void SetBindings() {
                    base.SetBindings();
                    Bind<TestSignal>();
                    On<TestSignal>().SwitchContext<TestContext>();
                }
            }
            public class DispatchSignalTestContext : Context {
                protected override void SetBindings() {
                    base.SetBindings();
                    Bind<TestSignal>();
                    On<TestSignal>().Dispatch<TestSignal>();
                }
            }
            public class AddContextTestContext : Context {
                protected override void SetBindings() {
                    base.SetBindings();
                    Bind<TestSignal>();
                    On<TestSignal>().AddContext<TestContext>();
                }
            }
            public class ParentTestContext : Context {
                protected override void SetBindings() {
                    base.SetBindings();
                    Bind<TestSignal>();
                }
            }
            public class ChildTestContext : Context {
                protected override void SetBindings() {
                    base.SetBindings();
                    Bind<TestSignal>();
                }
            }
            public class OneExecutionParameterCommandTestContext : Context {
                public const string PARAMETER1_VALUE = "test from context with 1 parameter";
                protected override void SetBindings() {
                    base.SetBindings();
                    On<TestSignal>().Do<OneExecuteParameterCommand>(PARAMETER1_VALUE);
                }
            }
            public class TwoExecutionParametersCommandTestContext : Context {
                public const string PARAMETER1_VALUE = "test from context with 2 parameters";
                public const int PARAMETER2_VALUE = 1337;
                protected override void SetBindings() {
                    base.SetBindings();
                    On<TestSignal>().Do<TwoExecuteParametersCommand>(PARAMETER1_VALUE, PARAMETER2_VALUE);
                }
            }
            public class ThreeExecutionParametersCommandTestContext : Context {
                public const string PARAMETER1_VALUE = "test from context with 3 parameters";
                public const int PARAMETER2_VALUE = 1010;
                public static readonly Command PARAMETER3_VALUE = new TestCommand();
                protected override void SetBindings() {
                    base.SetBindings();
                    On<TestSignal>().Do<ThreeExecuteParametersCommand>(PARAMETER1_VALUE, PARAMETER2_VALUE, PARAMETER3_VALUE);
                }
            }
            public class TwoExecutionParametersGivenTooManyCommandTestContext : Context {
                public const string PARAMETER1_VALUE = "test from context with too many parameters";
                public const int PARAMETER2_VALUE = 9001;
                public static readonly Command PARAMETER3_VALUE = new TestCommand();
                protected override void SetBindings() {
                    base.SetBindings();
                    On<TestSignal>().Do<TwoExecuteParametersCommand>(PARAMETER1_VALUE, PARAMETER2_VALUE, PARAMETER3_VALUE);
                }
            }
            public class TwoExecutionParametersGivenTooFewCommandTestContext : Context {
                public const string PARAMETER1_VALUE = "test from context with too little parameters";
                protected override void SetBindings() {
                    base.SetBindings();
                    On<TestSignal>().Do<TwoExecuteParametersCommand>(PARAMETER1_VALUE);
                }
            }
            public class CommandChainBranchingTestContext : Context {
                protected override void SetBindings() {
                    base.SetBindings();
                    On<TestSignal>().Do<TestCommand>();
                    On<TestSignal>().Do<TestCommand>();
                }
            }
            public class CommandChainBranchingAbortTestCommand : Command<bool> {
                protected override void Execute(bool abort) {
                    if (abort) { Abort(); }
                }
            }
            public class CommandChainBranchingAbortTestContext : Context {
                protected override void SetBindings() {
                    base.SetBindings();
                    On<TestSignal>().Do<CommandChainBranchingAbortTestCommand>(true)
                                    .Do<TestCommand>();
                    On<TestSignal>().Do<CommandChainBranchingAbortTestCommand>(false)
                                    .Do<TestCommand>();
                }
            }
        }

        [Test]
        public void ContextHasNoCommandBindings_OnConstruction() {
            //Act
            TestContext context = new TestContext();

            //Assert
            Assert.That(context.CommandBindings.Count() == 0);
        }

        [Test]
        public void ContextSubclassBindsCommand_OnInitialize() {
            //Arrange
            TestTypes.TestContext context = new TestTypes.TestContext();

            //Act
            context.Initialize();

            //Assert
            Assert.That(context.CommandBindings.Any(x => x.Key.GetType() == typeof(TestTypes.TestSignal)));
        }

        [Test]
        public void ContextSubclassBindsCommandToSignal_OnInitialize() {
            //Arrange
            TestTypes.TestContext context = new TestTypes.TestContext();

            //Act
            context.Initialize();

            //Assert
            Assert.That(context.CommandBindings.Any(x => x.Value[0].Commands.Count == 1 &&
                                                        x.Value[0].Commands[0].GetType() == typeof(TestTypes.TestCommand)));
        }

        [Test]
        public void ContextSubclassBindsGotoStateCommand_OnInitialize() {
            //Arrange
            TestTypes.GotoStateTestContext context = new TestTypes.GotoStateTestContext();

            //Act
            context.Initialize();

            //Assert
            Assert.That(context.CommandBindings.Any(x => x.Value[0].Commands.Count == 1 &&
                                                        x.Value[0].Commands[0].GetType() == typeof(GotoStateCommand<TestTypes.TestContext>)));
        }

        [Test]
        public void ContextSubclassBindsSwitchContextCommand_OnInitialize() {
            //Arrange
            TestTypes.SwitchContextTestContext context = new TestTypes.SwitchContextTestContext();

            //Act
            context.Initialize();

            //Assert
            Assert.That(context.CommandBindings.Any(x => x.Value[0].Commands.Count == 1 &&
                                                        x.Value[0].Commands[0].GetType() == typeof(SwitchContextCommand<TestTypes.TestContext>)));
        }

        [Test]
        public void ContextSubclassBindsDispatchSignalCommand_OnInitialize() {
            //Arrange
            TestTypes.DispatchSignalTestContext context = new TestTypes.DispatchSignalTestContext();

            //Act
            context.Initialize();

            //Assert
            Assert.That(context.CommandBindings.Any(x => x.Value[0].Commands.Count == 1 &&
                                                        x.Value[0].Commands[0].GetType() == typeof(DispatchSignalCommand<TestTypes.TestSignal>)));
        }

        [Test]
        public void ContextSubclassBindsAddContextCommand_OnInitialize() {
            //Arrange
            TestTypes.AddContextTestContext context = new TestTypes.AddContextTestContext();

            //Act
            context.Initialize();

            //Assert
            Assert.That(context.CommandBindings.Any(x => x.Value[0].Commands.Count == 1 &&
                                                        x.Value[0].Commands[0].GetType() == typeof(AddContextCommand<TestTypes.TestContext>)));
        }

        [Test]
        public void ContextExecutesCommand_OnSignalDispatch() {
            //Arrange
            TestTypes.TestContext context = new TestTypes.TestContext();
            context.Initialize();

            KeyValuePair<Type, object> signalInjectionBinding = context.InjectionBindings.FirstOrDefault(x => x.Key == typeof(TestTypes.TestSignal));
            TestTypes.TestSignal signal = signalInjectionBinding.Value as TestTypes.TestSignal;

            KeyValuePair<AbstractSignal, List<SignalResponse>> commandBinding = context.CommandBindings.FirstOrDefault(x => x.Key.GetType() == typeof(TestTypes.TestSignal));
            TestTypes.TestCommand command = commandBinding.Value[0].Commands[0] as TestTypes.TestCommand;

            //Assert
            Assert.That(!command.IsExecuted);

            //Act
            signal.Dispatch();

            //Assert
            Assert.That(command.IsExecuted);
        }

        [Test]
        public void RemovedContextDoesNotExecuteCommand_OnSignalDispatch() {
            //Arrange
            TestTypes.TestContext context = new TestTypes.TestContext();
            context.Initialize();

            KeyValuePair<Type, object> signalInjectionBinding = context.InjectionBindings.FirstOrDefault(x => x.Key == typeof(TestTypes.TestSignal));
            TestTypes.TestSignal signal = signalInjectionBinding.Value as TestTypes.TestSignal;

            KeyValuePair<AbstractSignal, List<SignalResponse>> commandBinding = context.CommandBindings.FirstOrDefault(x => x.Key.GetType() == typeof(TestTypes.TestSignal));
            TestTypes.TestCommand command = commandBinding.Value[0].Commands[0] as TestTypes.TestCommand;

            //Assert
            Assert.That(!command.IsExecuted);

            context.Remove();

            //Act
            signal.Dispatch();

            //Assert
            Assert.That(!command.IsExecuted);
        }

        [Test]
        public void ChildContextSignalDoesNotDispatchParentContextSignal_OnSignalDispatch() {
            //Arrange
            TestTypes.ParentTestContext parentContext = new TestTypes.ParentTestContext();
            parentContext.Initialize();
            Signal parentSignal = parentContext.InjectionBindings.FirstOrDefault(x => x.Key == typeof(TestTypes.TestSignal)).Value as Signal;

            TestTypes.ChildTestContext childContext = parentContext.InstantiateContext<TestTypes.ChildTestContext>() as TestTypes.ChildTestContext;
            Signal childSignal = childContext.InjectionBindings.FirstOrDefault(x => x.Key == typeof(TestTypes.TestSignal)).Value as Signal;

            //Act
            childSignal.Dispatch();

            //Assert
            Assert.That(parentSignal.DispatchCount == 0);
        }

        [Test]
        public void ContextExecutesCommandWithOneExecutionParameter_OnSignalDispatch() {
            //Arrange
            TestTypes.OneExecutionParameterCommandTestContext context = new TestTypes.OneExecutionParameterCommandTestContext();
            context.Initialize();
            Signal signal = context.InjectionBindings.FirstOrDefault(x => x.Key == typeof(TestTypes.TestSignal)).Value as Signal;

            TestTypes.OneExecuteParameterCommand command = (TestTypes.OneExecuteParameterCommand)context.CommandBindings.First().Value[0].Commands.First();

            //Assert
            Assert.That(command.Parameter1 == null);

            //Act
            signal.Dispatch();

            //Assert
            Assert.That(command.Parameter1 == TestTypes.OneExecutionParameterCommandTestContext.PARAMETER1_VALUE);
        }

        [Test]
        public void ContextExecutesCommandWithTwoExecutionParameter_OnSignalDispatch() {
            //Arrange
            TestTypes.TwoExecutionParametersCommandTestContext context = new TestTypes.TwoExecutionParametersCommandTestContext();
            context.Initialize();
            Signal signal = context.InjectionBindings.FirstOrDefault(x => x.Key == typeof(TestTypes.TestSignal)).Value as Signal;

            TestTypes.TwoExecuteParametersCommand command = (TestTypes.TwoExecuteParametersCommand)context.CommandBindings.First().Value[0].Commands.First();

            //Assert
            Assert.That(command.Parameter1 == null);
            Assert.That(command.Parameter2 == 0);

            //Act
            signal.Dispatch();

            //Assert
            Assert.That(command.Parameter1 == TestTypes.TwoExecutionParametersCommandTestContext.PARAMETER1_VALUE);
            Assert.That(command.Parameter2 == TestTypes.TwoExecutionParametersCommandTestContext.PARAMETER2_VALUE);
        }

        [Test]
        public void ContextExecutesCommandWithThreeExecutionParameter_OnSignalDispatch() {
            //Arrange
            TestTypes.ThreeExecutionParametersCommandTestContext context = new TestTypes.ThreeExecutionParametersCommandTestContext();
            context.Initialize();
            Signal signal = context.InjectionBindings.FirstOrDefault(x => x.Key == typeof(TestTypes.TestSignal)).Value as Signal;

            TestTypes.ThreeExecuteParametersCommand command = (TestTypes.ThreeExecuteParametersCommand)context.CommandBindings.First().Value[0].Commands.First();

            //Assert
            Assert.That(command.Parameter1 == null);
            Assert.That(command.Parameter2 == 0);
            Assert.That(command.Parameter3 == null);

            //Act
            signal.Dispatch();

            //Assert
            Assert.That(command.Parameter1 == TestTypes.ThreeExecutionParametersCommandTestContext.PARAMETER1_VALUE);
            Assert.That(command.Parameter2 == TestTypes.ThreeExecutionParametersCommandTestContext.PARAMETER2_VALUE);
            Assert.That(command.Parameter3 == TestTypes.ThreeExecutionParametersCommandTestContext.PARAMETER3_VALUE);
        }

        [Test]
        public void ContextExecutesCommandWithTooManyExecutionParametersGiven_OnSignalDispatch() {
            //Arrange
            TestTypes.TwoExecutionParametersGivenTooManyCommandTestContext context = new TestTypes.TwoExecutionParametersGivenTooManyCommandTestContext();
            context.Initialize();
            Signal signal = context.InjectionBindings.FirstOrDefault(x => x.Key == typeof(TestTypes.TestSignal)).Value as Signal;

            TestTypes.TwoExecuteParametersCommand command = (TestTypes.TwoExecuteParametersCommand)context.CommandBindings.First().Value[0].Commands.First();

            //Assert
            Assert.That(command.Parameter1 == null);
            Assert.That(command.Parameter2 == 0);

            //Act
            signal.Dispatch();

            //Assert
            Assert.That(command.Parameter1 == TestTypes.TwoExecutionParametersGivenTooManyCommandTestContext.PARAMETER1_VALUE);
            Assert.That(command.Parameter2 == TestTypes.TwoExecutionParametersGivenTooManyCommandTestContext.PARAMETER2_VALUE);
        }

        [Test]
        public void ContextDoesNotExecuteCommandWithTooFewExecutionParametersGiven_OnSignalDispatch() {
            //Arrange
            TestTypes.TwoExecutionParametersGivenTooFewCommandTestContext context = new TestTypes.TwoExecutionParametersGivenTooFewCommandTestContext();
            context.Initialize();
            Signal signal = context.InjectionBindings.FirstOrDefault(x => x.Key == typeof(TestTypes.TestSignal)).Value as Signal;

            TestTypes.TwoExecuteParametersCommand command = (TestTypes.TwoExecuteParametersCommand)context.CommandBindings.First().Value[0].Commands.First();

            //Assert
            Assert.That(command.Parameter1 == null);
            Assert.That(command.Parameter2 == 0);

            //Act
            signal.Dispatch();

            //Assert
            Assert.That(command.Parameter1 == null);
            Assert.That(command.Parameter2 == 0);
        }

        [Test]
        public void ContextBranchesCommandChain_OnInitialize() {
            //Arrange
            TestTypes.CommandChainBranchingTestContext context = new TestTypes.CommandChainBranchingTestContext();

            //Act
            context.Initialize();

            //Assert
            Assert.That(context.CommandBindings.First().Value.Count == 2);
        }

        [Test]
        public void BranchedCommandChainExecutesAllCommands_OnDispatch() {
            //Arrange
            TestTypes.CommandChainBranchingTestContext context = new TestTypes.CommandChainBranchingTestContext();
            context.Initialize();

            Signal signal = context.InjectionBindings.FirstOrDefault(x => x.Key == typeof(TestTypes.TestSignal)).Value as Signal;

            TestTypes.TestCommand commandInFirstChain = (TestTypes.TestCommand)context.CommandBindings.First().Value[0].Commands[0];
            TestTypes.TestCommand commandInSecondChain = (TestTypes.TestCommand)context.CommandBindings.First().Value[1].Commands[0];

            //Act
            signal.Dispatch();

            //Assert
            Assert.That(commandInFirstChain.IsExecuted);
            Assert.That(commandInSecondChain.IsExecuted);
        }

        [Test]
        public void ContextDoesNotAbortAllCommandChainBranches_OnAbortInOneCommandChain() {
            //Arrange
            TestTypes.CommandChainBranchingAbortTestContext context = new TestTypes.CommandChainBranchingAbortTestContext();
            context.Initialize();
            Signal signal = context.InjectionBindings.FirstOrDefault(x => x.Key == typeof(TestTypes.TestSignal)).Value as Signal;

            TestTypes.TestCommand testCommandInAbortedCommandChain = (TestTypes.TestCommand)context.CommandBindings.First().Value[0].Commands[1];
            TestTypes.TestCommand testCommandInUnabortedCommandChain = (TestTypes.TestCommand)context.CommandBindings.First().Value[1].Commands[1];

            //Act
            signal.Dispatch();

            //Assert
            Assert.That(!testCommandInAbortedCommandChain.IsExecuted);
            Assert.That(testCommandInUnabortedCommandChain.IsExecuted);
        }

    }

}