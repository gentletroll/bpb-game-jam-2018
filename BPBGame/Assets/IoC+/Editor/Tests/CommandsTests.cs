/// <copyright file="CommandsTests.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using NSubstitute;
using NUnit.Framework;

namespace IoCPlus {

    public class CommandsTests {

        private class TestContext : Context { }
        private class TestSignal : Signal { }
        private class TestView : View { }

        [Test]
        public void GotoStateCommandSetsContextState_OnExecute() {
            //Arrange
            GotoStateCommand<TestContext> command = new GotoStateCommand<TestContext>();

            DynamicContext dynamicContext = new DynamicContext();
            IContext context = Substitute.For<Context>();
            dynamicContext.Bind<IContext>(context);

            dynamicContext.InjectInto(command);

            //Act
            command.PerformExecution();

            //Assert
            context.Received(1).SetContextState<TestContext>();
        }

        [Test]
        public void SwitchStateCommandSwitchesContextState_OnExecute() {
            //Arrange
            SwitchStateCommand<TestContext> command = new SwitchStateCommand<TestContext>();

            DynamicContext dynamicContext = new DynamicContext();
            IContext context = Substitute.For<Context>();
            dynamicContext.Bind<IContext>(context);

            dynamicContext.InjectInto(command);

            //Act
            command.PerformExecution();

            //Assert
            context.Received(1).SwitchContextState<TestContext>();
        }

        [Test]
        public void SwitchContextCommandSwitchesContext_OnExecute() {
            //Arrange
            SwitchContextCommand<TestContext> command = new SwitchContextCommand<TestContext>();

            DynamicContext dynamicContext = new DynamicContext();
            IContext context = Substitute.For<Context>();
            dynamicContext.Bind<IContext>(context);

            dynamicContext.InjectInto(command);

            //Act
            command.PerformExecution();

            //Assert
            context.Received(1).Switch<TestContext>();
        }

        [Test]
        public void AddContextCommandAddsContext_OnExecute() {
            //Arrange
            AddContextCommand<TestContext> command = new AddContextCommand<TestContext>();

            DynamicContext dynamicContext = new DynamicContext();
            IContext context = Substitute.For<Context>();
            dynamicContext.Bind<IContext>(context);

            dynamicContext.InjectInto(command);

            //Act
            command.PerformExecution();

            //Assert
            context.Received(1).InstantiateContext<TestContext>();
        }

        [Test]
        public void DispatchSignalCommandDispatchesSignal_OnExecute() {
            //Arrange
            DispatchSignalCommand<TestSignal> command = new DispatchSignalCommand<TestSignal>();

            DynamicContext dynamicContext = new DynamicContext();
            TestSignal signal = dynamicContext.Bind<TestSignal>();

            dynamicContext.InjectInto(command);

            //Assert
            Assert.That(signal.DispatchCount == 0);

            //Act
            command.PerformExecution();

            //Assert
            Assert.That(signal.DispatchCount == 1);
        }

        [Test]
        public void InstantiateViewCommandInstantiatesView_OnExecute() {
            //Arrange
            InstantiateViewCommand<TestView> command = new InstantiateViewCommand<TestView>();

            DynamicContext dynamicContext = new DynamicContext();
            IContext context = Substitute.For<IContext>();
            dynamicContext.Bind<IContext>((context));

            dynamicContext.InjectInto(command);

            //Act
            command.PerformExecution();

            //Assert
            context.Received(1).InstantiateView<TestView>();
        }

    }

}