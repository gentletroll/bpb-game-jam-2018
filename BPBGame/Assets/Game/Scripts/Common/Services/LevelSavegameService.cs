using Core.Services;
using Game.Scripts.Common.Models;

namespace Game.Scripts.Common.Services
{
    public class LevelSavegameService : SettingsService<LevelSavegame>
    {
        public override string SettingsFileName 
        {
            get { return "Level.json"; }
        }
    }
}