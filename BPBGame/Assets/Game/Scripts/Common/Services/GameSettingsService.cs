using Core.Services;
using Game.Scripts.Common.Models;

namespace Game.Scripts.Common.Services
{
    public class GameSettingsService : SettingsService<GameSettings>, IGameSettingService
    {
        public override string SettingsFileName 
        {
            get { return "Game.json"; }
        }
    }
}