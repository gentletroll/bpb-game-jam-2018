using Game.Scripts.Common.Models;

namespace Game.Scripts.Common.Services
{
    public interface IGameSettingService
    {
        GameSettings Load();
        void Save(GameSettings settings);
    }
}