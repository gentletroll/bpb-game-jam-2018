using IoCPlus;

namespace Game.Scripts.Common.Signals
{
    public class MusicVolumeChangedSignal : Signal<float>
    {                    
    }
}