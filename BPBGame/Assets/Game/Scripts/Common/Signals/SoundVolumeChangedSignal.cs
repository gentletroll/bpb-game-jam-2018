using IoCPlus;

namespace Game.Scripts.Common.Signals
{
    public class SoundVolumeChangedSignal : Signal<float>
    {                    
    }
}