using Game.Scripts.Common.Configuration;
using General.Commands;

namespace Game.Scripts.Common.Commands
{
    public class PlayChangeLanguageSoundCommand : PlayOneShotAudioBaseCommand
    {
        public override string ClipName { get { return AudioClipName.ChangeLanguage; } }
        public override string Category { get { return AudioSourceCategory.Sound; } }
    }
}
