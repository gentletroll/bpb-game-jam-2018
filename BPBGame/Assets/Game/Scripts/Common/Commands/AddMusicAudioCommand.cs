using Game.Scripts.Common.Views;
using General.Commands;

namespace Game.Scripts.Common.Commands
{
    public class AddMusicAudioCommand : AddAudioSourceCommand<MusicAudioView>
    {
        public override string AudioSourcePrefab
        {
            get { return "MusicAudioSource"; }
        }
    }
}
