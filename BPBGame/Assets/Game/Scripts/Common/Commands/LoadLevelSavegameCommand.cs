using System;
using Game.Scripts.Common.Models;
using Game.Scripts.Common.Services;
using IoCPlus;

namespace Game.Scripts.Common.Commands
{
    public class LoadLevelSavegameCommand : Command
    {
        [Inject]
        LevelSavegame _savegame;

        [Inject]
        LevelSavegameService _levelSavegameService;

        protected override void Execute()
        {
            try
            {
                var savegame = _levelSavegameService.Load();
                _savegame.Started = savegame.Started;
            }
            catch (Exception exception)
            {
                UnityEngine.Debug.Log(exception.Message);
            }
        }
    }
}
