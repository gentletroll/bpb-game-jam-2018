using Game.Scripts.Common.Models;
using IoCPlus;

namespace Game.Scripts.Common.Commands
{
    public class SetLevelStartedCommand : Command
    {
        [Inject]
        LevelSavegame _savegame;

        protected override void Execute()
        {
            _savegame.Started = true;
        }
    }
}
