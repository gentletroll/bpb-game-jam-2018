using System;
using Game.Scripts.Common.Models;
using Game.Scripts.Common.Services;
using IoCPlus;

namespace Game.Scripts.Common.Commands
{
    public class LoadGameSettingsCommand : Command
    {
        [Inject]
        GameSettings _settings;

        [Inject]
        IGameSettingService _gameSettingsService;

        protected override void Execute()
        {
            try
            {
                var settings = _gameSettingsService.Load();
                 _settings.TextLanguage.Value = settings.TextLanguage.Value;
                 _settings.MusicVolume.Value = settings.MusicVolume.Value; 
                 _settings.SoundVolume.Value = settings.SoundVolume.Value;
            }
            catch (Exception exception)
            {
                // TODO we could create a new file here (e.g. read from a TextAsset)
                UnityEngine.Debug.Log(exception.Message);
            }
        }        
    }
}
