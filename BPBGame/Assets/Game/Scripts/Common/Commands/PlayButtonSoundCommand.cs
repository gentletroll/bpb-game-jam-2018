using Game.Scripts.Common.Configuration;
using General.Commands;

namespace Game.Scripts.Common.Commands
{
    public class PlayButtonSoundCommand : PlayOneShotAudioBaseCommand
    {
        public override string ClipName { get { return AudioClipName.Button; } }
        public override string Category { get { return AudioSourceCategory.Sound; } }
    }
}
