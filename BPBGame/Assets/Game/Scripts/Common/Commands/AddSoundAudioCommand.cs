using Game.Scripts.Common.Views;
using General.Commands;

namespace Game.Scripts.Common.Commands
{
    public class AddSoundAudioCommand : AddAudioSourceCommand<SoundAudioView>
    {
        public override string AudioSourcePrefab
        {
            get { return "SoundAudioSource"; }
        }
    }
}