using Game.Scripts.Common.Models;
using Game.Scripts.Common.Services;
using IoCPlus;

namespace Game.Scripts.Common.Commands
{
    public class SaveLevelSaveGameCommand : Command
    {
        [Inject]
        LevelSavegame _levelSaveGame;

        [Inject]
        LevelSavegameService _levelSaveGameService;

        protected override void Execute()
        {
            _levelSaveGameService.Save(_levelSaveGame);
        }        
    }
}
