using Game.Scripts.Common.Models;
using Game.Scripts.Common.Services;
using IoCPlus;

namespace Game.Scripts.Common.Commands
{
    public class SaveGameSettingsCommand : Command
    {
        [Inject]
        GameSettings _settings;

        [Inject]
        IGameSettingService _gameSettingsService;

        protected override void Execute()
        {
            _gameSettingsService.Save(_settings);
        }        
    }
}
