
using Core.Interfaces;
using UniRx;
using UnityEngine;

namespace Game.Scripts.Common.Models
{
    public class GameSettings : ILanguageSettings
    {
        public ReactiveProperty<string> TextLanguage { get; set; }

        public ReactiveProperty<float> MusicVolume { get; set; }
        public ReactiveProperty<float> SoundVolume { get; set; }
        
        public int PlayerStartTokens { get; internal set; }
        public int NeededPlayers { get; internal set; }

        public Color[] AgendaColors { get; internal set; }
        public Sprite[] AgendaIcons { get; internal set; }
    }
}