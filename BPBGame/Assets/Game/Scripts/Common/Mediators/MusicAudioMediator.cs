using Core.Extensions;
using Game.Scripts.Common.Models;
using General.Views;
using IoCPlus;
using UniRx;

namespace Game.Scripts.Common.Mediators
{
    public class MusicAudioMediator : Mediator<IAudioSourceView>
    {
        [Inject]
        GameSettings _gameSettings;

        public override void Initialize()
        {
            _gameSettings.MusicVolume
                .Select(x => x.Clamp(0.0f, 1.0f))
                .Do(x => view.Volume = x)
                .Subscribe();
        }

        public override void Dispose()
        {
        }
    }
}