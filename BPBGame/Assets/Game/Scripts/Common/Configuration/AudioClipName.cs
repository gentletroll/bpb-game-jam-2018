namespace Game.Scripts.Common.Configuration
{
    public static class AudioClipName
    {
        public const string Button = "MenuButton";
        public const string ChangeLanguage = "Level-Select";
        public const string MainMenuMusic = "Levemap-Loop";
    }
}