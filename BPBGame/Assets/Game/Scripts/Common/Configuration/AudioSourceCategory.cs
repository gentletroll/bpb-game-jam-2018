namespace Game.Scripts.Common.Configuration
{
    public static class AudioSourceCategory
    {
        public const string Music = "Music";
        public const string Sound = "Sound";
    }
}