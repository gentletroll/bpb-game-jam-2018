using Core.Interfaces;
using Core.Repositories;
using Core.Services;
using Core.Util;
using Game.Scripts.Common.Commands;
using Game.Scripts.Common.Mediators;
using Game.Scripts.Common.Models;
using Game.Scripts.Common.Services;
using Game.Scripts.Common.Views;
using General.GameContext;
using IoCPlus;
using UniRx;
using UnityEngine;

namespace Game.Scripts.Common.Contexts
{
    public abstract class GameContext : GameContextBase
    {
        protected override void SetBindings()        
        {
            base.SetBindings();

            // add some basic game settings
            var gameSettings = new GameSettings
            {
                TextLanguage = new ReactiveProperty<string>(LanguageCodes.German),
                MusicVolume = new ReactiveProperty<float>(.5f),
                SoundVolume = new ReactiveProperty<float>(.5f),

                PlayerStartTokens = 15,
                NeededPlayers = 3,
                 
                AgendaColors = new Color[]
                {
                    new Color(150f / 255f, 200f / 255f, 115f / 255f), 
                    new Color(92f / 255f, 232f / 255f, 200f / 255f), 
                    new Color(249f / 255f, 136f / 255f, 45f / 255f), 
                    new Color(244f / 255f, 200f / 255f, 0f / 255f), 
                    new Color(169f / 255f, 92f / 255f, 175f / 255f), 
                    new Color(100f / 255f, 159f / 255f, 255f / 255f)
                }//,

                /*AgendaIcons = new Sprite[] 
                {
                    Resources.Load<Sprite>()
                }*/
            };

            // add some dummy sample savegame
            var levelSavegame = new LevelSavegame
            {
                Started = false
            };

            // models
            Bind<GameSettings>(gameSettings);
            Bind<ILanguageSettings>(gameSettings);

            Bind<LevelSavegame>(levelSavegame);

            // views
            BindMediator<MusicAudioMediator, MusicAudioView>();
            BindMediator<SoundAudioMediator, SoundAudioView>();

            // services
            Bind<ITextService>(new TextService(new TextRepository(), gameSettings));
            Bind<IAudioService>(new AudioService(new ResourcesAudioClipRepository()));
            Bind<IGameSettingService, GameSettingsService>();
            Bind<LevelSavegameService>();  
            
            On<EnterContextSignal>()
                .Do<AddMusicAudioCommand>()
                .Do<AddSoundAudioCommand>();
        }
    }
}