using HappyFunTimes;

namespace Game.Scripts.HTFBridge
{
    public class PlayerRemovedMessage
    {
        public NetPlayer NetPlayer { get; private set; }

        public PlayerRemovedMessage(NetPlayer netPlayer)
        {
            NetPlayer = netPlayer;
        }
    }
}