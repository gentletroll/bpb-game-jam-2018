using System;
using Game.Scripts.MainScene.Commands;
using HappyFunTimes;
using IoCPlus;
using UniRx;
using UnityEngine;

namespace Game.Scripts.HTFBridge
{
        public class PlayerBridge : MonoBehaviour
    {
        public float speed = 4.0f;
        private HFTInput _input;
        private NetPlayer _netPlayer;

        void Start() 
        {
            _input = GetComponent<HFTInput>();            
        }

        void Update() 
        {
            float dx = speed * (_input.GetAxis("Horizontal") + Input.GetAxis("Horizontal")) * Time.deltaTime;
            float dz = speed * (_input.GetAxis("Vertical")   + Input.GetAxis("Vertical")  ) * Time.deltaTime;
            transform.position = transform.position + new Vector3(dx, 0.0f, dz);
        }   

        void InitializeNetPlayer(HappyFunTimes.SpawnInfo spawnInfo) 
        {   
            MessageBroker.Default.Publish(new PlayerJoinedMessage(spawnInfo.netPlayer, spawnInfo.data));   

            spawnInfo.netPlayer.OnDisconnect += Remove;

            _netPlayer = spawnInfo.netPlayer;

            spawnInfo.netPlayer.RegisterCmdHandler<MessageVote>("vote", OnCharacter);


    /*     // Save the netplayer object so we can use it send messages to the phone
        m_netPlayer = spawnInfo.netPlayer;

        // Register handler to call if the player disconnects from the game.
        m_netPlayer.OnDisconnect += Remove;

        // Track name changes
        m_playerNameManager = new HFTPlayerNameManager(m_netPlayer);

        // Setup events for the different messages.
        m_netPlayer.RegisterCmdHandler<MessageCharacter>("character", OnCharacter); */
        }

        private void OnCharacter(MessageVote voteData)
        {
            MessageBroker.Default.Publish(new PlayerVotedMessage(_netPlayer, voteData.tokens));   
        }

        private void Remove(object sender, EventArgs e)
        {
            NetPlayer netPlayer = (NetPlayer)sender;
            MessageBroker.Default.Publish(message: new PlayerRemovedMessage(netPlayer));  
            Destroy(gameObject);
        }
    }
}