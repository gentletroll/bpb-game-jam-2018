using HappyFunTimes;

namespace Game.Scripts.HTFBridge
{
    public class PlayerJoinedMessage
    {
        public NetPlayer NetPlayer { get; private set; }
        public object Data { get; private set; }

        public PlayerJoinedMessage(NetPlayer netPlayer, object data)
        {
            NetPlayer = netPlayer;
            Data = data;
        }
    }
}