using HappyFunTimes;

namespace Game.Scripts.HTFBridge
{
    public class PlayerVotedMessage
    {
        public NetPlayer NetPlayer { get; private set; }
        public int Tokens { get; private set; }

        public PlayerVotedMessage(NetPlayer netPlayer, int tokens)
        {
            NetPlayer = netPlayer;
            Tokens = tokens;
        }
    }
}