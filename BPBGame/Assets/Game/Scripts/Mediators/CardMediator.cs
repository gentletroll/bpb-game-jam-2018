﻿using System.Linq;
using System.Runtime.InteropServices;
using Assets.Game.Scripts.Models;
using Assets.Game.Scripts.Signals;
using Assets.Game.Scripts.Views;
using Game.Scripts.Common.Models;
using Game.Scripts.MainScene.Commands;
using Game.Scripts.MainScene.Signals;
using IoCPlus;
using UnityEngine;

namespace Assets.Game.Scripts.Mediators
{

    public class CardMediator : Mediator<ICardView>
    {
        [Inject] private VotingFinishedSignal _votingFinishedSignal;
        [Inject] private CardDrawnSignal _cardDrawnSignal;
        [Inject] private CardHiddenSignal _cardHiddenSignal;
        //[Inject] private CardApprovedSignal _cardApprovedSignal;
        //[Inject] private CardDeclinedSignal _cardDeclinedSignal;
        [Inject] private GameSettings _gameSettings;
        [Inject] private GameModel _gameModel;

        public override void Initialize()
        {
            _cardDrawnSignal.AddListener(RenderAndShowNewCard);
            //_cardApprovedSignal.AddListener(HideCard);
            //_cardDeclinedSignal.AddListener(HideCard);
            _votingFinishedSignal.AddListener(TurnCard);
            view.OnCardHidden.AddListener(HandleCardHidden);
        }

        private void TurnCard()
        {
            int upVotesCount = 0;
            int downVotesCount = 0;
            _gameModel.Votes.ForEach(vote =>
            {
                if (vote.Tokens > 0)
                    upVotesCount += vote.Tokens;
                else
                    downVotesCount -= vote.Tokens;
            });
            view.SetVotes(upVotesCount, downVotesCount);
            view.Turn();
        }

        public void HideCard()
        {
            view.Hide();
        }

        public override void Dispose()
        {

        }

        private void RenderAndShowNewCard(CardModel model)
        {
            view.SetCard(model, _gameSettings);
            view.Show();
        }

        private void HandleCardHidden()
        {
            _cardHiddenSignal.Dispatch();
        }
    }
}
