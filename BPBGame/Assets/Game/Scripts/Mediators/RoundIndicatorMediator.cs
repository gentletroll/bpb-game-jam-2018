﻿using Assets.Game.Scripts.Models;
using Assets.Game.Scripts.Services;
using Assets.Game.Scripts.Signals;
using Assets.Game.Scripts.Views;
using Game.Scripts.MainScene.Signals;
using IoCPlus;

namespace Assets.Game.Scripts.Mediators
{
    public class RoundIndicatorMediator : Mediator<IRoundIndicatorView>
    {
        [Inject] private CardDrawnSignal _cardDrawnSignal;
        [Inject] private StartGameSignal _startGameSignal;
        [Inject] private UpdateRoundDisplaySignal _newRoundDisplaySignal;
        [Inject] private ICardsService _cardsService;

        public int CardsPlayed { get; private set; }
        public int Round { get; private set; }

        public override void Initialize()
        {
            _cardDrawnSignal.AddListener(RegisterCardPlayed);
            _startGameSignal.AddListener(UpdateTotalRounds);
            _newRoundDisplaySignal.AddListener(UpdateTotalRounds);
        }

        public override void Dispose()
        {
            _cardDrawnSignal.RemoveListener(RegisterCardPlayed);
            _startGameSignal.RemoveListener(UpdateTotalRounds);
            _newRoundDisplaySignal.RemoveListener(UpdateTotalRounds);
        }

        private void UpdateTotalRounds()
        {
            CardsPlayed = 0;
            var totalCards = _cardsService.CardsStack.Count;
            view.SetTotalRounds(totalCards);
        }

        private void RegisterCardPlayed(CardModel cardModel)
        {
            CardsPlayed++;
            view.SetRound(CardsPlayed);
        }
    }
}
