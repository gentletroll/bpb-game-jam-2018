using System;
using Assets.Game.Scripts.Signals;
using IoCPlus;

namespace Assets.Game.Scripts.Mediators
{
    public class NextRoundMediator : Mediator<NextRoundView>
    {
        [Inject] NewRoundSignal _newRoundSignal;        

        public override void Initialize()
        {
            _newRoundSignal.AddListener(Do);
        }
        
        public override void Dispose()
        {            
        }

        private void Do()
        {
            view.TriggerNextRoundOverlay();
        }
    }
}
