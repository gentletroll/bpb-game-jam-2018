using System.Collections.Generic;
using System.Linq;
using Assets.Game.Scripts.Views;
using IoCPlus;
using UniRx;

namespace Assets.Game.Scripts.Mediators
{
    public class TickerMediator : Mediator<TickerView>
    {
        List<string> _list = new List<string>
        {
            "Versuchstiere heiß begehrt bei Tierliebhabern",
            "Stromnetz unter Druck nach AKW-Abschaltungen?",
            "Bauland wird rar und teuer",
            "Stille Städte und klare Luft nach Verbrennungsmotorenverbot",
            "Wütende Proteste von Bauern und Holzwirtschaft",
            "Pferd verhaftet: erster Kriminalfall nach Ausweitung von Menschenrechten auf Tiere",
            "Betreten verboten: Es wird einsam im Wald",
            "Fernweh wird schmerzhaft: Malle ganz weit weg",
            "Leben unter Glass: Bevölkerung verständnissvoll aber bedrückt"
        };

        public override void Initialize()
        {
/*             var lala = _list.Join(" ");

            var index = 0;

            Observable.EveryUpdate().Subscribe(x => 
            {
                var lasasd = lala.First();
                view.TickerText.text += lasasd;
                lala.Remove(0);
            });
 */        }

        public override void Dispose()
        {
        }
    }
}
