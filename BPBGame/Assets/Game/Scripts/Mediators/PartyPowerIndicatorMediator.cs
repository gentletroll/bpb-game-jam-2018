using System;
using Assets.Game.Scripts.Models;
using Game.Scripts.Common.Models;
using Game.Scripts.MainScene.Signals;
using IoCPlus;

namespace Assets.Game.Scripts.Mediators
{
    public class PartyPowerIndicatorMediator : Mediator<IPowerIndicatorView>
    {
        [Inject] NewPlayerCreatedSignal _newPlayerCreatedSignal;
        [Inject] UpdatePlayerProgessSignal _updatePlayerProgessSignal;
        [Inject] private GameSettings _gameSettings;
        
        public override void Initialize()
        {
            _newPlayerCreatedSignal.AddListener(OnPlayerCreated);
            _updatePlayerProgessSignal.AddListener(OnPlayerUpdated);
        }

        public override void Dispose()
        {
            _newPlayerCreatedSignal.RemoveListener(OnPlayerCreated);
            _updatePlayerProgessSignal.RemoveListener(OnPlayerUpdated);
        }

        private void OnPlayerCreated(PlayerModel player)
        {
            view.SetPlayer(player);                
        }

        private void OnPlayerUpdated(PlayerModel player)
        {
            var agenda1Color = _gameSettings.AgendaColors[(int) player.Agenda1];
            var agenda2Color = _gameSettings.AgendaColors[(int) player.Agenda2];
            view.UpdatePlayer(player, agenda1Color, agenda2Color);                
        }
    }
}
