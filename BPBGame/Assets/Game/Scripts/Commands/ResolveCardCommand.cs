﻿using System.Linq;
using Assets.Game.Scripts.Enums;
using Assets.Game.Scripts.Mediators;
using Assets.Game.Scripts.Models;
using Assets.Game.Scripts.Services;
using IoCPlus;
using UnityEngine;

namespace Assets.Game.Scripts.Commands
{
    public class ResolveCardCommand : Command
    {
        [Inject] private ICardsService _cardsService;
        [Inject] private GameModel _gameModel;

        protected override void Execute()
        {
            var card = _cardsService.CurrentCard;
            ResolvePrimaryAgenda(card);
            ResolveSecondaryAgenda(card);

            _cardsService.TakeCardOutOfGame(_cardsService.CurrentCard);
        }

        private void ResolvePrimaryAgenda(CardModel card)
        {
            var agenda = card.PrimaryAgenda;
            _gameModel.Players.ForEach(x => { Debug.Log(x.Agenda1 + " / " + x.Agenda2);});
            var player = GetPlayerByAgenda(agenda);
            Debug.Log("player = " + player + " agenda = " + agenda);
            if (player.Agenda1 == agenda)
                player.ProgressAgenda1++;
            else if (player.Agenda2 == agenda)
                player.ProgressAgenda2++;
        }

        private void ResolveSecondaryAgenda(CardModel card)
        {
            var agenda = card.SecondaryAgenda;
            bool hasSecondaryEffect = agenda != Agenda.None && card.SecondaryTokenModifier != 0;
            if (hasSecondaryEffect)
            {
                var player = GetPlayerByAgenda(agenda);
                if (player != null)
                {
                    player.Tokens += card.SecondaryTokenModifier;
                    if (player.Tokens < 0)
                        player.Tokens = 0;
                }                    
            }
        }

        private PlayerModel GetPlayerByAgenda(Agenda agenda)
        {
            return _gameModel.Players.FirstOrDefault(x => x.Agenda1 == agenda || x.Agenda2 == agenda);
        }
    }
}