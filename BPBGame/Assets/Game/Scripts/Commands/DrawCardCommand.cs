﻿using Assets.Game.Scripts.Services;
using Assets.Game.Scripts.Signals;
using IoCPlus;

namespace Assets.Game.Scripts.Commands
{
    public class DrawCardCommand : Command
    {
        [Inject] private ICardsService _cardsService;
        [Inject] private CardDrawnSignal _cardDrawnSignal;

        protected override void Execute()
        {
            _cardDrawnSignal.Dispatch(_cardsService.DrawCard());
        }
    }
}
