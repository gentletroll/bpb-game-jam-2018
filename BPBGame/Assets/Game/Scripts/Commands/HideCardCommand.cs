﻿using Assets.Game.Scripts.Mediators;
using Assets.Game.Scripts.Services;
using IoCPlus;

namespace Assets.Game.Scripts.Commands
{
    public class HideCardCommand : Command
    {
        [Inject] private CardMediator _cardMediator;

        protected override void Execute()
        {
            _cardMediator.HideCard();
        }
    }
}
