﻿using Assets.Game.Scripts.Services;
using Assets.Game.Scripts.Signals;
using IoCPlus;

namespace Assets.Game.Scripts.Commands
{
    public class CheckForRoundCompleteCommand : Command
    {
        [Inject] private ICardsService _cardsService;
        [Inject] private NewRoundSignal _newRoundSignal;
        [Inject] private DrawCardSignal _drawCardSignal;

        protected override void Execute()
        {
            if (_cardsService.CardsStack.Count <= 0)
                _newRoundSignal.Dispatch();
            else
                _drawCardSignal.Dispatch();
        }
    }
}
