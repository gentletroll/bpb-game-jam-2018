﻿using Assets.Game.Scripts.Models;
using Assets.Game.Scripts.Services;
using Game.Scripts.Common.Models;
using IoCPlus;

namespace Assets.Game.Scripts.Commands
{
    public class RefillCardsStackCommand : Command
    {
        [Inject] private ICardsService _cardsService;
        [Inject] private GameModel _gameModel;
        [Inject] private GameSettings _gameSettings;

        protected override void Execute()
        {
            _gameModel.Players.ForEach(x => {
                x.Tokens = _gameSettings.PlayerStartTokens;
            });
            _cardsService.RefillCardsStack();
        }
    }
}
