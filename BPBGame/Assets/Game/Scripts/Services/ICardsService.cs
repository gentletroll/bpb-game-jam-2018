﻿using System.Collections.Generic;
using Assets.Game.Scripts.Models;

namespace Assets.Game.Scripts.Services
{
    public interface ICardsService
    {
        List<CardModel> AllCards { get; }
        List<CardModel> RemainingCards { get; }
        List<CardModel> CardsGraveyard { get; }
        List<CardModel> CardsStack { get; }
        List<CardModel> PlayedCardsStack { get; }
        CardModel CurrentCard { get; }
        //int CardsDrawn { get; }

        CardModel DrawCard();
        void TakeCardOutOfGame(CardModel card);
        void RefillCardsStack();
    }
}