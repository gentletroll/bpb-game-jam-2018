﻿using Assets.Game.Scripts.Models;
using Newtonsoft.Json;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Assets.Game.Scripts.Enums;


namespace Assets.Game.Scripts.Services
{
    public class CardsService : ICardsService
    {
        public List<CardModel> AllCards { get; private set; }
        public List<CardModel> RemainingCards { get { return AllCards.Where(x => x.IsInPlay).ToList(); } }
        public List<CardModel> CardsGraveyard { get { return AllCards.Where(x => !x.IsInPlay).ToList(); } }
        public List<CardModel> CardsStack { get; private set; }
        public List<CardModel> PlayedCardsStack { get; private set; }
        public CardModel CurrentCard { get; private set; }
        //public int CardsDrawn { get; private set; }

        public CardsService()
        {
            var json = Resources.Load<TextAsset>("Configuration/cards");
            AllCards = ParseCardsJSON(json);
            SetupCardsStack();
        }

        public CardModel DrawCard()
        {
            if (CardsStack.Count <= 0) return null;
                
            var card = GetRandomCardFromList(CardsStack);
            CurrentCard = card;
            //CardsDrawn++;
            CardsStack.Remove(card);
            PlayedCardsStack.Add(card);
            return card;
        }

        public void TakeCardOutOfGame(CardModel card)
        {
            card.IsInPlay = false;
            RemoveCardFromList(card, CardsStack);
            RemoveCardFromList(card, PlayedCardsStack);
        }

        public void RefillCardsStack()
        {
            foreach (var card in PlayedCardsStack)
            {
                CardsStack.Add(card);
            }
            PlayedCardsStack.Clear();

            for (int i = 0; i < (int)Agenda.Length; i++)
                HandleProgressOfAgenda((Agenda)i);

            //RemoveRandomCardsFromStack(2);
        }

        private List<CardModel> ParseCardsJSON(TextAsset json)
        {
            var models = JsonConvert.DeserializeObject<IEnumerable<CardModel>>(json.text);
            return models.ToList();
        }

        private void SetupCardsStack()
        {
            CardsStack = AllCards.Where(x => x.AgendaProgress <= 2).ToList();
            PlayedCardsStack = new List<CardModel>();
            //RemoveRandomCardsFromStack(4);      
        }

        private void RemoveRandomCardsFromStack(int numberOfCards)
        {
            for (int i = 0; i < numberOfCards; i++)
            {
                if (CardsStack.Count == 0) return;

                var card = GetRandomCardFromList(CardsStack);
                CardsStack.Remove(card);
                PlayedCardsStack.Add(card);
            }
        }

        private CardModel GetRandomCardFromList(List<CardModel> list)
        {
            var randomIndex = Mathf.RoundToInt(Random.Range(-0.49f, list.Count - 0.51f));
            return list[randomIndex];
        }

        private void HandleProgressOfAgenda(Agenda agenda)
        {
            if (GetProgressOfAgenda(agenda) == 8 && !HasStackCard(agenda, 9))
            {
                CardsStack.Add(GetCard(agenda, 9));
            }
            else if (GetProgressOfAgenda(agenda) == 6 && !HasStackCard(agenda, 7))
            {
                CardsStack.Add(GetCard(agenda, 7));
                CardsStack.Add(GetCard(agenda, 8));
            }
            else if (GetProgressOfAgenda(agenda) == 4 && !HasStackCard(agenda, 5))
            {
                CardsStack.Add(GetCard(agenda, 5));
                CardsStack.Add(GetCard(agenda, 6));
            }
            else if (GetProgressOfAgenda(agenda) == 2 && !HasStackCard(agenda, 3))
            {
                CardsStack.Add(GetCard(agenda, 3));
                CardsStack.Add(GetCard(agenda, 4));
            }
            /*if (GetProgressOfAgenda(agenda) == 6 && !HasStackCard(agenda, 7))
            {
                CardsStack.Add(GetCard(agenda, 7));
                CardsStack.Add(GetCard(agenda, 8));
                CardsStack.Add(GetCard(agenda, 9));
            }
            else if (GetProgressOfAgenda(agenda) == 3 && !HasStackCard(agenda, 4))
            {
                CardsStack.Add(GetCard(agenda, 4));
                CardsStack.Add(GetCard(agenda, 5));
                CardsStack.Add(GetCard(agenda, 6));
            }*/
        }

        private bool HasStackCard(Agenda agenda, int progress)
        {
            return CardsStack.Any(x => x.PrimaryAgenda == agenda && x.AgendaProgress == progress);
        }

        private CardModel GetCard(Agenda agenda, int progress)
        {
            return AllCards.First(x => x.PrimaryAgenda == agenda && x.AgendaProgress == progress);
        }

        private int GetProgressOfAgenda(Agenda agenda)
        {
            var resolvedAgendaCards = CardsGraveyard.Where(x => x.PrimaryAgenda == agenda).ToList();
            var progress = 0;
            for (int i = 1; i <= 9; i++)
            {
                if (resolvedAgendaCards.Any(x => x.AgendaProgress == i))
                    progress = i;
                else
                    break;
            }
            return progress;
        }

        private void RemoveCardFromList(CardModel card, List<CardModel> list)
        {
            if (list.Contains(card))
                list.Remove(card);
        }
     
    }
}
