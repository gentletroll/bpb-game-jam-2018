using Assets.Game.Scripts.Enums;

namespace Assets.Game.Scripts.Models
{
    public class AgendaPair
    {
        public Agenda A1 { get; set; }
        public Agenda A2 { get; set; }

        public AgendaPair(Agenda a1, Agenda a2)
        {
            A1 = a1;
            A2 = a2;
                    }
    }
}