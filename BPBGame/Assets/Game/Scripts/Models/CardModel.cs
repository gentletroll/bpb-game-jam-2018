﻿using Assets.Game.Scripts.Enums;

namespace Assets.Game.Scripts.Models
{
    public class CardModel
    {
        public string Id;
        public Agenda PrimaryAgenda;
        public Agenda SecondaryAgenda;
        public int AgendaProgress;
        public int SecondaryTokenModifier;
        public string Title;

        // Dynamic fields
        public bool IsInPlay = true;
    }
}
