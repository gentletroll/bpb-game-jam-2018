using System;
using System.Collections.Generic;
using Assets.Game.Scripts.Enums;
using Game.Scripts.MainScene.Commands;
using HappyFunTimes;

namespace Assets.Game.Scripts.Models
{
    public class PlayerModel
    {
        public string Id { get; set; }

        public int Index { get; set; }

        public int Tokens { get; set; }

        public NetPlayer NetPlayer { get; set; }

        public Agenda Agenda1 { get; internal set; }
        public Agenda Agenda2 { get; internal set; }

        public int ProgressAgenda1 { get; internal set; }
        public int ProgressAgenda2 { get; internal set; }

        public override string ToString()
        {
            return "Player: " + Id + " " + Tokens + " " + Agenda1 + " " + Agenda2;
        }
    }
}