using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Game.Scripts.Enums;
using Core.Extensions;

namespace Assets.Game.Scripts.Models
{
    public class RandomAgendaHelper
    {
         List<Agenda> _agendaList = new List<Agenda>
            {
                Agenda.Biopolitics,                    
                Agenda.Economy,
                Agenda.Migration,
                Agenda.Nature,
                Agenda.Security,
                Agenda.Tech
            }; 

              List<AgendaPair> _mappingAgendas = new List<AgendaPair>
		{ 
                new AgendaPair(Agenda.Biopolitics, Agenda.Economy),
                new AgendaPair(Agenda.Biopolitics, Agenda.Migration),
                new AgendaPair(Agenda.Biopolitics, Agenda.Security),
                new AgendaPair(Agenda.Biopolitics, Agenda.Tech),


                new AgendaPair(Agenda.Economy, Agenda.Migration),
                new AgendaPair(Agenda.Economy, Agenda.Security),
                new AgendaPair(Agenda.Economy, Agenda.Tech),

                new AgendaPair(Agenda.Migration, Agenda.Tech),

                new AgendaPair(Agenda.Nature, Agenda.Biopolitics),
                new AgendaPair(Agenda.Nature, Agenda.Economy),
                new AgendaPair(Agenda.Nature, Agenda.Migration),
                new AgendaPair(Agenda.Nature, Agenda.Security),
                new AgendaPair(Agenda.Nature, Agenda.Tech),

                new AgendaPair(Agenda.Security, Agenda.Migration),
                new AgendaPair(Agenda.Security, Agenda.Tech)
		};

        public AgendaPair FindAgendaPair(Agenda a1, Agenda a2)
        {
            return _mappingAgendas.Where(x => (x.A1 == a1 || x.A2 == a1) && (x.A1 == a2 || x.A2 == a2)).First();
        }

        public Agenda PopAgenda()
        {
            var shuffledList = _agendaList.Randomize();

            var lastEntry = _agendaList.Last();
            _agendaList.Remove(lastEntry);

            return lastEntry;
        }
    }
}