using System;
using System.Collections.Generic;
using Core.Extensions;
using Game.Scripts.MainScene.Commands;

namespace Assets.Game.Scripts.Models
{
    public class GameModel
    {
        public GameModel()
        {
            Players = new List<PlayerModel>();
            Votes = new List<Vote>();
        }

        public List<PlayerModel> Players { get; }
        public List<Vote> Votes { get; }

        public void AddVote(Vote vote)
        {
            Votes.Add(vote);
        }

        public void ClearVotes()
        {
            Votes.Clear();
        }
    }
}