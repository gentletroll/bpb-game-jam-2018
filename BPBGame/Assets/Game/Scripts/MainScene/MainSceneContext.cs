using Assets.Game.Scripts.Commands;
using Assets.Game.Scripts.Mediators;
using Assets.Game.Scripts.Services;
using Assets.Game.Scripts.Signals;
using Assets.Game.Scripts.Views;
using Game.Scripts.Common.Commands;
using Game.Scripts.Common.Contexts;
using General.Commands;
using General.Mediators;
using General.Signals;
using General.Views;
using IoCPlus;
using UnityEngine;
using UniRx;
using Assets.Game.Scripts.Models;
using Game.Scripts.MainScene.Signals;
using Game.Scripts.HTFBridge;
using Game.Scripts.MainScene.Commands;

namespace Game.Scripts.MainMenu
{
    public class MainSceneContext : GameContext
    {
        protected override void SetBindings()
        {
            base.SetBindings();

            // models 
            Bind<GameModel>(new GameModel());
            Bind<RandomAgendaHelper>(new RandomAgendaHelper());

            // signals
            Bind<TextLanguageWasChangedSignal>();
            Bind<UpdatePlayerInfoRemoteSignal>();
            Bind<DrawCardSignal>();
            Bind<CardDrawnSignal>();
            Bind<UpdatePlayerProgessSignal>();
            Bind<UpdateRoundDisplaySignal>();            

            Bind<CardApprovedSignal>();
            Bind<CardDeclinedSignal>();
            Bind<CardHiddenSignal>();
            Bind<NewRoundSignal>();
            Bind<NewPlayerCreatedSignal>();

            var playerJoinedSignal = Bind<PlayerJoinedSignal>();
            var playerRemovedSignal = Bind<PlayerRemovedSignal>();
            var playerVotedSignal = Bind<PlayerVotedSignal>();

            MessageBroker.Default.Receive<PlayerJoinedMessage>()
                .Subscribe(x => 
                {                    
                    playerJoinedSignal.Dispatch(x.NetPlayer);
                });           

            MessageBroker.Default.Receive<PlayerRemovedMessage>()
                .Subscribe(x => 
                {                    
                    playerRemovedSignal.Dispatch(x.NetPlayer);
                });           

            MessageBroker.Default.Receive<PlayerVotedMessage>()
                .Subscribe(x => 
                {                    
                    playerVotedSignal.Dispatch(x.NetPlayer, x.Tokens);
                });           

            // views
            BindMediator<SystemInfoMediator, SystemInfoView>();
            BindMediator<CardMediator, CardView>();
            BindMediator<PartyPowerIndicatorMediator, PartyPowerIndicatorView>();
            BindMediator<RoundIndicatorMediator, RoundIndicatorView>();
            BindMediator<TickerMediator, TickerView>();
            BindMediator<NextRoundMediator, NextRoundView>();

            // services
            Bind<ICardsService>(new CardsService());

            On<EnterContextSignal>()
                .Do<InstantiateUICanvasCommand>()
                .Do<InstantiateViewCommand>("PartyPowerIndicator-0")
                .Do<InstantiateViewCommand>("PartyPowerIndicator-1")
                .Do<InstantiateViewCommand>("PartyPowerIndicator-2")
                .Do<InstantiateViewCommand>("Card")
                .Do<InstantiateViewCommand>("Roundindicator")
                .Do<InstantiateViewCommand>("Next round");
                //.Do<InstantiateViewCommand>("TickerText");

            On<PlayerJoinedSignal>()
                .Do<NewPlayerJoinedCommand>();

            On<PlayerRemovedSignal>()
                .Do<PlayerRemovedCommand>();                

            On<PlayerVotedSignal>()
                .Do<PlayerVotedCommand>();

            On<UpdatePlayerInfoRemoteSignal>()
                .Do<UpdatePlayerInfoRemoteCommand>();

            On<StartGameSignal>()
                .Do<UpdatePlayersInfoRemoteCommand>()
                .Do<DrawCardCommand>();

            On<VotingFinishedSignal>()
                .Do<VotingFinishedCommand>();

            On<CardApprovedSignal>()
                .Do<ResolveCardCommand>()
                .Do<UpdatePartyPowerIndicatorCommand>();
            //.Do<HideCardCommand>();

            /*On<CardDeclinedSignal>()
                .Do<HideCardCommand>();*/

            On<DrawCardSignal>()
                .Do<DrawCardCommand>();

            On<CardHiddenSignal>()
                .Do<UpdatePlayersInfoRemoteCommand>()
                .Do<CheckForRoundCompleteCommand>();

            On<NewRoundSignal>()
                .Do<RefillCardsStackCommand>()
                .Dispatch<UpdateRoundDisplaySignal>()
                .Do<UpdatePlayersInfoRemoteCommand>()
                .Do<DrawCardCommand>();         
        }
    }
}

