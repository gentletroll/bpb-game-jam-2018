using HappyFunTimes;
using IoCPlus;

namespace Game.Scripts.MainScene.Signals
{
    public class PlayerRemovedSignal : Signal<NetPlayer>
    {        
    }
}