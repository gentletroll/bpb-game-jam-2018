using Assets.Game.Scripts.Models;
using IoCPlus;

namespace Game.Scripts.MainScene.Signals
{
    public class NewPlayerCreatedSignal : Signal<PlayerModel>
    {        
    }
}