using HappyFunTimes;
using IoCPlus;

namespace Game.Scripts.MainScene.Signals
{
    public class PlayerVotedSignal : Signal<NetPlayer, int>
    {        
    }
}