using HappyFunTimes;
using IoCPlus;

namespace Game.Scripts.MainScene.Signals
{
    public class PlayerJoinedSignal : Signal<NetPlayer>
    {        
    }
}