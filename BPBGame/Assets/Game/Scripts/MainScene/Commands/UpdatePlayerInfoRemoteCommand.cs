using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Game.Scripts.Models;
using Core.Services;
using IoCPlus;

namespace Game.Scripts.MainScene.Commands
{
    public class UpdatePlayerInfoRemoteCommand : Command
    {
        [Inject] GameModel _gameModel;
        [Inject] ITextService _textService;

        [InjectParameter] string _sessionId;

          List<string> _mapping = new List<string>
		{ 
			"square", 
			"gem", 
			"circle"
		};

	    List<string> _mapping1 = new List<string>
		{ 
			"biopolitics_economy", 
			"biopolitics_migration", 
			"biopolitics_security",
            "biopolitics_tech",
            "economy_migration",
            "economy_security",
            "economy_tech",
            "migration_tech",
            "nature_biopolitics",
            "nature_economy",
            "nature_migration",
            "nature_security",
            "nature_tech",
            "security_migration",
            "security_tech"
		};

        protected override void Execute()
        {
            var player = _gameModel.Players.SingleOrDefault(x => x.Id == _sessionId);

            if (player == null)
                throw new Exception("Could not find player with session id" + _sessionId);

            player.NetPlayer.SendCmd("setTokens", new MessageSetTokens(player.Tokens));

            player.NetPlayer.SendCmd(
                "setAgenda1", 
                new MessageSetAgenda(
                    player.Agenda1.ToString().ToLower(),
                    _textService.GetById(player.Agenda1.ToString()+ "_Headline"),
                    _textService.GetById(player.Agenda1.ToString()+ "_Text")));

            player.NetPlayer.SendCmd(
                "setAgenda2",
                new MessageSetAgenda(
                    player.Agenda2.ToString().ToLower(),
                    _textService.GetById(player.Agenda2.ToString()+ "_Headline"),
                    _textService.GetById(player.Agenda2.ToString()+ "_Text")));

                // TEst
                var playderId = _mapping[player.Index];

                var lala = _mapping1.Single(y => y.Contains(player.Agenda1.ToString().ToLower()) && y.Contains(player.Agenda2.ToString().ToLower()));

                player.NetPlayer.SendCmd("onBegin", new MessageSetParty(playderId + "_" + lala));
                player.NetPlayer.SendCmd("setTokens", new MessageSetTokens(player.Tokens));

        }
    }
}