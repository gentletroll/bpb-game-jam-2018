using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Game.Scripts.Models;
using IoCPlus;

namespace Game.Scripts.MainScene.Commands
{
    public class UpdatePlayersInfoRemoteCommand : Command
    {
        [Inject] GameModel _gameModel;

	    List<string> _mapping = new List<string>
		{ 
			"square", 
			"gem", 
			"circle"
		};

	    List<string> _mapping1 = new List<string>
		{ 
			"biopolitics_economy", 
			"biopolitics_migration", 
			"biopolitics_security",
            "biopolitics_tech",
            "economy_migration",
            "economy_security",
            "economy_tech",
            "migration_tech",
            "nature_biopolitics",
            "nature_economy",
            "nature_migration",
            "nature_security",
            "nature_tech",
            "security_migration",
            "security_tech"
		};

        protected override void Execute()
        {
            _gameModel.Players.ForEach(x => 
                {
                    var playderId = _mapping[x.Index];

                    var lala = _mapping1.Single(y => y.Contains(x.Agenda1.ToString().ToLower()) && y.Contains(x.Agenda2.ToString().ToLower()));

                    x.NetPlayer.SendCmd("onBegin", new MessageSetParty(playderId + "_" + lala));
                    x.NetPlayer.SendCmd("setTokens", new MessageSetTokens(x.Tokens));
                });

            _gameModel.ClearVotes();
        }            
    }
}