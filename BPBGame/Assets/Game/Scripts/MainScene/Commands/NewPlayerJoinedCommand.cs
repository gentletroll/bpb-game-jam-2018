using System.Linq;
using Assets.Game.Scripts.Models;
using Game.Scripts.Common.Models;
using Game.Scripts.MainScene.Signals;
using HappyFunTimes;
using IoCPlus;
using UnityEngine;

namespace Game.Scripts.MainScene.Commands
{
    public class NewPlayerJoinedCommand : Command
    {
        [Inject] GameModel _gameModel;
        [Inject] GameSettings _gameSettings;
        [Inject] RandomAgendaHelper _randomAgendaHelper;

        [Inject] UpdatePlayerInfoRemoteSignal _updateRemoteSignal;
        [Inject] NewPlayerCreatedSignal _newPlayerCreatedSignal;
        [Inject] UpdatePlayerProgessSignal _testSignal;

        [Inject] StartGameSignal _startGameSignal;

        [InjectParameter]  NetPlayer _netPlayer;

        protected override void Execute()
        {
            UnityEngine.Debug.Log("New player " + _netPlayer.GetSessionId());

            var alala = _gameModel.Players.SingleOrDefault(x => x.Id == _netPlayer.GetSessionId());

            if (alala != null)
            {
                alala.NetPlayer = _netPlayer;
                Debug.Log("Already logged in id " + alala.Id);
                _updateRemoteSignal.Dispatch(alala.Id);
                return;
            }

            var a1 = _randomAgendaHelper.PopAgenda();
            var a2 = _randomAgendaHelper.PopAgenda();

            var pair = _randomAgendaHelper.FindAgendaPair(a1, a2);

            // get random agenda
            var player = new PlayerModel
            {
                Id = _netPlayer.GetSessionId(),
                Index = _gameModel.Players.Count,
                NetPlayer = _netPlayer,
                Tokens = _gameSettings.PlayerStartTokens, 
                Agenda1 = pair.A1,
                Agenda2 = pair.A2
            };

            _gameModel.Players.Add(player);         
            
            _updateRemoteSignal.Dispatch(player.Id);
            _newPlayerCreatedSignal.Dispatch(player);

            // TEST ONLY!!!!!
/*             player.ProgressAgenda1 = 3;
            player.ProgressAgenda2 = 8;
            _testSignal.Dispatch(player);
 */
            if (_gameModel.Players.Count == _gameSettings.NeededPlayers)            
                _startGameSignal.Dispatch();            
        }
    }
}