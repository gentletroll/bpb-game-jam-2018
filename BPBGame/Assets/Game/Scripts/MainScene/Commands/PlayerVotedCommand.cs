using System;
using System.Linq;
using Assets.Game.Scripts.Models;
using Game.Scripts.Common.Models;
using Game.Scripts.MainScene.Signals;
using HappyFunTimes;
using IoCPlus;

namespace Game.Scripts.MainScene.Commands
{
    public class PlayerVotedCommand : Command
    {
        [Inject] GameModel _gameModel;
        [Inject] GameSettings _gameSettings;
        [Inject] VotingFinishedSignal _votingFinishedSignal;

        [InjectParameter]  NetPlayer _netPlayer;
        [InjectParameter]  int _tokens;

        protected override void Execute()
        {
            UnityEngine.Debug.Log(_netPlayer.GetSessionId() + " " + _tokens);

            var player = _gameModel.Players.SingleOrDefault(x => x.Id == _netPlayer.GetSessionId());

            if (player == null)
                throw new Exception("Could not find player with session id" + _netPlayer.GetSessionId());

            _gameModel.AddVote(new Vote(player.Id, _tokens));           

            if (_gameModel.Votes.Count == _gameSettings.NeededPlayers)
                _votingFinishedSignal.Dispatch();           
        }
    }
}