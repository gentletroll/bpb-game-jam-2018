namespace Game.Scripts.MainScene.Commands
{
    public class MessageSetAgenda
     {
        public MessageSetAgenda(string value1, string value2, string value3) 
        {
            agenda = value1;
            headline = value2;
            text = value3;
        }

        public string headline;
        public string text;
        public string agenda;
    }
}