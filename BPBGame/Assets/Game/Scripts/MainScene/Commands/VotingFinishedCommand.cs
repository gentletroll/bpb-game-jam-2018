using System;
using System.Linq;
using Assets.Game.Scripts.Models;
using Assets.Game.Scripts.Signals;
using IoCPlus;

namespace Game.Scripts.MainScene.Commands
{
    public class VotingFinishedCommand : Command
    {
        [Inject] private GameModel _gameModel;
        [Inject] private CardApprovedSignal _cardApprovedSignal;
        [Inject] private CardDeclinedSignal _cardDeclinedSignal;

        protected override void Execute()
        {
            UnityEngine.Debug.Log("VotingFinishedCommandVotingFinishedCommandVotingFinishedCommand");
            int votesBalance = 0;
            _gameModel.Votes.ForEach(x =>
            {
                var player = _gameModel.Players.SingleOrDefault(y => y.Id == x.PlayerId);
                player.Tokens -= Math.Abs(x.Tokens);

                if (player.Tokens < 0)
                    player.Tokens = 0;

                votesBalance += x.Tokens;
            });
            if (votesBalance > 0)
                _cardApprovedSignal.Dispatch();
            else            
                _cardDeclinedSignal.Dispatch();
        }
    }
}