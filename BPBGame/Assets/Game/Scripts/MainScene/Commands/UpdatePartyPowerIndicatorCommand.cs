using Assets.Game.Scripts.Models;
using Game.Scripts.MainScene.Signals;
using IoCPlus;

namespace Game.Scripts.MainScene.Commands
{
    public class UpdatePartyPowerIndicatorCommand : Command
    {
        [Inject] GameModel _gameModel;
        [Inject] UpdatePlayerProgessSignal _updatePlayerProgessSignal;

        protected override void Execute()
        {
            _gameModel.Players.ForEach(x => 
            {
                _updatePlayerProgessSignal.Dispatch(x);
            });
        }                    
    }
}