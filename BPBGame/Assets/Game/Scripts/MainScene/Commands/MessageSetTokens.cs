namespace Game.Scripts.MainScene.Commands
{
    public class MessageSetTokens
     {
        public MessageSetTokens(int value) 
        {
            tokens = value;
        }

        public int tokens;
    }
}