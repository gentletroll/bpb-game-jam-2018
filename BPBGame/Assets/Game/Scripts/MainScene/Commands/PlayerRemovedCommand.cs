using HappyFunTimes;
using IoCPlus;

namespace Game.Scripts.MainScene.Commands
{
    public class PlayerRemovedCommand : Command
    {
        [InjectParameter]  NetPlayer _netPlayer;

        protected override void Execute()
        {
            UnityEngine.Debug.Log("Removed player " + _netPlayer.GetSessionId());
        }
    }
}