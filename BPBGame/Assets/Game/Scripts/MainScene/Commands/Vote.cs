namespace Game.Scripts.MainScene.Commands
{
    public class Vote
    {
        private int _tokens;
        public int Tokens { get { return _tokens; } }
        private string _playerId;
        public string PlayerId { get { return _playerId; } }

        public Vote(string playerId, int tokens)        
        {
            _tokens = tokens;
            _playerId = playerId;
        }
    }
}