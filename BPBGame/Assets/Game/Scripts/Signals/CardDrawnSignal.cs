﻿using Assets.Game.Scripts.Models;
using IoCPlus;

namespace Assets.Game.Scripts.Signals
{
    public class CardDrawnSignal : Signal<CardModel>
    {
    }
}
