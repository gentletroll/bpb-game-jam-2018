﻿using IoCPlus;

namespace Assets.Game.Scripts.Signals
{
    public class NewRoundSignal : Signal
    {
    }

    public class UpdateRoundDisplaySignal : Signal
    {
    }
}
