using Assets.Game.Scripts.Models;
using IoCPlus;
using UnityEngine;

public interface IPowerIndicatorView : IView
{    
    int Index { get; }

    void SetPlayer(PlayerModel playerModel);
    void UpdatePlayer(PlayerModel playerModel, Color agenda1Color, Color agenda2Color);
}