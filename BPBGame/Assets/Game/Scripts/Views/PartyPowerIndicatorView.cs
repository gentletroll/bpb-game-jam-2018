﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Game.Scripts.Models;
using IoCPlus;
using UnityEngine;
using UnityEngine.UI;

public class PartyPowerIndicatorView : View, IPowerIndicatorView
{
	[SerializeField] private Image _partyIcon;
	[SerializeField] private Image _leftFillBar;

	[SerializeField] private Image _rightFillBar;
	[SerializeField] private int _index;

    public int Index 
	{ 
		get { return _index; }
	}

	List<string> _mapping = new List<string>
		{ 
			"Party Icons-square", 
			"Party Icons-gem", 
			"Party Icons-circle"
		};

	private Sprite[] _sprites;

    private RectTransform _rectTransformLeft;
	private RectTransform _rectTransformRight;

    private float _rectMax;
    private float _leftMaxY;
    private float _rightMaxY;

    void Awake()
	{
		_rectTransformLeft = _leftFillBar.gameObject.GetComponent<RectTransform>();
		_rectTransformRight = _rightFillBar.gameObject.GetComponent<RectTransform>();
	}

    void Start () 
	{
		_sprites = Resources.LoadAll<Sprite>("PartyandPolitics");		
		var sprite = _sprites.SingleOrDefault(x => x.name == _mapping[Index] + "_default");
		_partyIcon.sprite = sprite;	

		_leftMaxY = _rectTransformLeft.offsetMax.y;
		_rightMaxY = _rectTransformRight.offsetMax.y;		

		_rectTransformLeft.offsetMax = new Vector2(40, 0);
		_rectTransformRight.offsetMax = new Vector2(40, 0);
	}

    public void UpdatePlayer(PlayerModel playerModel, Color agenda1Color, Color agenda2Color)
    {
		if (_index != playerModel.Index)
			return;

		var progressAgenda1 = playerModel.ProgressAgenda1;
		var progressAgenda2 = playerModel.ProgressAgenda2;

		var pg1 = (float)progressAgenda1 / 10.0f;
		var pg2 = (float)progressAgenda2 / 10.0f;

        _leftFillBar.color = agenda1Color;
        _rightFillBar.color = agenda2Color;

		_rectTransformLeft.offsetMax = new Vector2(40, _leftMaxY * pg1);
		_rectTransformRight.offsetMax = new Vector2(40, _rightMaxY * pg2);		
	}

    public void SetPlayer(PlayerModel playerModel)
    {
		if (_index != playerModel.Index)
			return;

		Debug.Log("SetPlayer " + playerModel.Id + " " + _index);

		var agenda1 = playerModel.Agenda1.ToString().ToLower();
		var agenda2 = playerModel.Agenda2.ToString().ToLower();

		var key1 = agenda1 + "_" + agenda2;
		var key2 = agenda2 + "_" + agenda1;

		var sprite = _sprites.SingleOrDefault(x => x.name == _mapping[Index] + "_" + key1);

		if (sprite != null)
		{
			_partyIcon.sprite = sprite;
			return;
		}

		 sprite = _sprites.SingleOrDefault(x => x.name == _mapping[Index] + "_" + key2);

		if (sprite == null)		
			return;
		
		_partyIcon.sprite = sprite;
    }
}
