﻿using System.Collections;
using System.Collections.Generic;
using IoCPlus;
using UnityEngine;

public class NextRoundView : View 
{
	[SerializeField] private Animator _animator;

	public void TriggerNextRoundOverlay()
	{
		_animator.SetTrigger("nextRound");
	}
}
