﻿using System.Collections;
using System.Collections.Generic;
using IoCPlus;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Game.Scripts.Views
{
    public class RoundIndicatorView : View, IRoundIndicatorView {

        [SerializeField] private Text _roundIndicatorText;

        private int _totalRounds;

        public void SetTotalRounds(int totalRounds)
        {
            _totalRounds = totalRounds;
        }

        public void SetRound(int round)
        {
            _roundIndicatorText.text = "Wahlrunde: " + round + " von " + _totalRounds;
        }
    }

    public interface IRoundIndicatorView : IView
    {
        void SetTotalRounds(int totalRounds);
        void SetRound(int round);
    }
}