using IoCPlus;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Game.Scripts.Views
{
    public class TickerView : View
    {
        [SerializeField] private Text _ticker;       

        public Text TickerText {get { return _ticker;} }
    }
}