﻿using System.Linq;
using Assets.Game.Scripts.Enums;
using Assets.Game.Scripts.Models;
using Game.Scripts.Common.Models;
using IoCPlus;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Game.Scripts.Views
{

    public class CardView : View, ICardView
    {
        private Signal _onCardHidden = new Signal();

        public Signal OnCardHidden
        {
            get { return _onCardHidden; }
        }

        [SerializeField] private Text _title;
        [SerializeField] private Image _illustration;
        [SerializeField] private Image _primaryIcon;
        [SerializeField] private Image _secondaryIcon;
        [SerializeField] private Text _primaryValue;
        [SerializeField] private Text _secondaryValue;
        [SerializeField] private Text _upVotes;
        [SerializeField] private Text _downVotes;

        private Animator _animator;
        private Sprite[] _iconSprites;

        /*private string[] _iconKeys =
        {
            "Party Card Icons-circle_nature",
            "Party Card Icons-circle_biopolitics",
            "Party Card Icons-circle_economy",
            "Party Card Icons-circle_security",
            "Party Card Icons-circle_migration",
            "Party Card Icons-circle_tech",
        };*/

        private string[] _iconKeys =
        {
            "Politics Icons-nature_full",
            "Politics Icons-biopolitics_full",
            "Politics Icons-economy_full",
            "Politics Icons-security_full",
            "Politics Icons-migration_full",
            "Politics Icons-tech_full",
        };

        void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        void Start()
        {
            _iconSprites = Resources.LoadAll<Sprite>("PartyandPolitics");
        }

        public void SetCard(CardModel model, GameSettings gameSettings)
        {
            _title.text = model.Title;
            _primaryValue.gameObject.SetActive(false);
            //_primaryValue.color = Color.white;
            //_primaryValue.text = model.AgendaProgress.ToString();
            bool hasSecondaryEffect = model.SecondaryAgenda != Agenda.None;
            _secondaryIcon.gameObject.SetActive(hasSecondaryEffect);
            _secondaryValue.gameObject.SetActive(hasSecondaryEffect);
            if (hasSecondaryEffect)
            {
                _secondaryValue.color = Color.black;//gameSettings.AgendaColors[(int) model.SecondaryAgenda];
                _secondaryValue.text = model.SecondaryTokenModifier.ToString();
            }
            var illustrationKey = "Illustrations/" + model.Id;
            var sprite = Resources.Load<Sprite>(illustrationKey);
            _illustration.sprite = sprite;


            _primaryIcon.sprite = _iconSprites.FirstOrDefault(x => x.name == _iconKeys[(int)model.PrimaryAgenda]);
            if (hasSecondaryEffect)
                _secondaryIcon.sprite = _iconSprites.FirstOrDefault(x => x.name == _iconKeys[(int)model.SecondaryAgenda]);
        }

        public void Show()
        {
            _animator.SetBool("show", true);
        }

        public void SetVotes(int upVotes, int downVotes)
        {
            _upVotes.text = upVotes.ToString();
            _downVotes.text = downVotes.ToString();
        }

        public void Turn()
        {
            _animator.SetTrigger("flip");
            Invoke("Hide", 5f);
        }

        public void Hide()
        {
            _animator.SetBool("show", false);
            Invoke("HandleHidden", 1.1f);
        }

        private void HandleHidden()
        {
            _onCardHidden.Dispatch();
        }
    }
}