using Assets.Game.Scripts.Models;
using Game.Scripts.Common.Models;
using IoCPlus;

namespace Assets.Game.Scripts.Views
{
    public interface ICardView : IView
    {
        Signal OnCardHidden { get; }
        void SetCard(CardModel model, GameSettings gameSettings);
        void Show();
        void SetVotes(int upVotes, int downVotes);
        void Turn();
        void Hide();
    }
}