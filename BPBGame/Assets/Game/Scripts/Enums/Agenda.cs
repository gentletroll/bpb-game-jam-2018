﻿namespace Assets.Game.Scripts.Enums
{
    public enum Agenda
    {
        Nature,
        Biopolitics,
        Economy,
        Security,
        Migration,
        Tech,
        Length,
        None = -1
    }
}
